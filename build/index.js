"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = require("dotenv");
(0, dotenv_1.config)();
const app_1 = require("./app/app");
(0, app_1.startServer)();
// const populateDb = async () => {
//   const admin = {
//     name: "admin",
//     email: "admin@gmail.com",
//     password: "admin1",
//     role: Roles.admin,
//   };
//   await authService.register(admin);
// };
// populateDb();
