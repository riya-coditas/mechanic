"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.permit = void 0;
const permit = (roles) => {
    return (req, res, next) => {
        try {
            const { role } = res.locals.tokenDecode;
            for (let ele of roles) {
                if (ele === role.toString())
                    return next();
            }
            return next({ message: "Unauthorised Access", statusCode: 401 });
        }
        catch (e) {
            next(e);
        }
    };
};
exports.permit = permit;
