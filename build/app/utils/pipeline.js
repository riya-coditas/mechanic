"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generatePipeline = void 0;
const generatePipeline = (query) => {
    let pipeline = [];
    const { sort, limit = 10, sortDir, page = 1 } = query, filter = __rest(query, ["sort", "limit", "sortDir", "page"]);
    let filterObject = {};
    for (let field in filter) {
        if (Array.isArray(filter[field])) {
            filterObject[field] = {
                $in: filter[field],
            };
        }
        else if (typeof filter[field] === "object") {
            let obj = {};
            for (let [key, value] of Object.entries(filter[field])) {
                obj[`$${key}`] = typeof value === "number" ? value : Number(value);
            }
            filterObject[field] = obj;
        }
        else {
            const parsedValue = isNaN(Number(filter[field]))
                ? filter[field]
                : Number(filter[field]);
            filterObject[field] = parsedValue;
        }
    }
    if (Object.keys(filterObject).length > 0) {
        pipeline.push({ $match: filterObject });
    }
    const sortObject = {};
    if (sort && sortDir) {
        sortObject[sort] = sortDir === "asc" ? 1 : -1;
        pipeline.push({ $sort: sortObject });
    }
    const skip = (page - 1) * limit;
    pipeline.push({ $skip: skip });
    pipeline.push({ $limit: Number(limit) });
    return pipeline;
};
exports.generatePipeline = generatePipeline;
