"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.connectToMongo = void 0;
const mongoose_1 = require("mongoose");
const connectToMongo = async () => {
    try {
        const { MONGO_CONNECTION } = process.env;
        await (0, mongoose_1.connect)(MONGO_CONNECTION || '');
        console.log('CONNECTED TO MONGODB');
        return true;
    }
    catch (e) {
        throw { message: "COULD NOT CONNECT TO MONGO DB" };
    }
};
exports.connectToMongo = connectToMongo;
