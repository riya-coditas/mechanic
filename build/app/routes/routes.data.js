"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.excludedPaths = exports.routes = void 0;
const routes_type_1 = require("./routes.type");
const route_provider_1 = __importDefault(require("../feature-module/route-provider"));
const authorize_1 = require("../utils/authorize");
exports.routes = [
    new routes_type_1.Routes("/auth", route_provider_1.default.authRouter),
    new routes_type_1.Routes("/users", route_provider_1.default.userRouter),
    new routes_type_1.Routes("/roles", route_provider_1.default.roleRouter),
    new routes_type_1.Routes("/product", route_provider_1.default.productRouter),
    new routes_type_1.Routes("/shop", route_provider_1.default.shopRouter),
    new routes_type_1.Routes("/sales", route_provider_1.default.saleRouter),
    new routes_type_1.Routes("/requirement", route_provider_1.default.requirementRouter),
    new routes_type_1.Routes("/rewards", route_provider_1.default.rewardRouter),
];
exports.excludedPaths = [
    new authorize_1.ExcludedPath("/auth/login", "POST"),
    new authorize_1.ExcludedPath("/auth/register", "POST"),
    new authorize_1.ExcludedPath("/shop/", "GET"),
    new authorize_1.ExcludedPath("/ratings/rating", "POST"),
    new authorize_1.ExcludedPath("/auth/refresh", "POST")
];
