"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sale_responses_1 = require("./sale.responses");
const sale_schema_1 = require("./sale.schema");
const pipeline_1 = require("../../utils/pipeline");
const create = (sale) => sale_schema_1.SaleModel.create(sale);
const findById = async (id) => {
    const sale = await sale_schema_1.SaleModel.findById(id);
    if (!sale) {
        throw sale_responses_1.SALE_RESPONSES.NOT_FOUND;
    }
    return sale;
};
const findByIdAndUpdate = (filter, update, options = {}) => {
    return sale_schema_1.SaleModel.findByIdAndUpdate(filter, update, Object.assign(Object.assign({}, options), { new: true }));
};
const find = async (query) => {
    try {
        const pipeline = (0, pipeline_1.generatePipeline)(query);
        return await sale_schema_1.SaleModel.aggregate([
            {
                $match: { isDeleted: false },
            },
            ...pipeline,
        ]);
    }
    catch (err) {
        throw { message: "something went wrong", e: err };
    }
};
const update = async (filter, update) => await sale_schema_1.SaleModel.updateOne(filter, update);
const ViewPendingSales = async () => {
    const saleRecords = await sale_schema_1.SaleModel.find({ isValidated: false, isRejected: false });
    const pendingRevenue = await sale_schema_1.SaleModel.aggregate([
        {
            $match: { isValidated: false, isRejected: false }
        },
        {
            $group: {
                _id: "",
                totalRevenue: { $sum: "$Amount" },
                totalPoints: { $sum: "$points" }
            }
        }
    ]);
    return { pendingRevenue, saleRecords };
};
const approveSale = async () => {
    const filter = { isValidated: false, isRejected: false };
    const update = { isValidated: true };
    const approve = await sale_schema_1.SaleModel.updateMany(filter, update);
    const CalculateRevenue = await sale_schema_1.SaleModel.aggregate([
        {
            $match: { isValidated: true },
        },
        {
            $group: {
                _id: "",
                totalRevenue: { $sum: "$Amount" },
            },
        },
    ]);
    const totalRevenue = CalculateRevenue[0].totalRevenue;
    return { approve, totalRevenue };
};
const rejectSale = async () => {
    const filter = { isValidated: false, isRejected: false };
    const update = { isRejected: true };
    const rejected = await sale_schema_1.SaleModel.updateMany(filter, update);
    return rejected;
};
const itemwise = async () => {
    const result = await sale_schema_1.SaleModel.aggregate([
        { $match: { isValidated: true } },
        { $group: {
                _id: "$product_id",
                product_name: { $first: "$product_name" },
                totalQuantity: { $sum: "$quantity" },
                shop_id: { $first: "$shopid" }
            }
        }, {
            $sort: { totalQuantity: -1 }
        }
    ]);
    return result;
};
exports.default = {
    create,
    findById,
    findByIdAndUpdate,
    ViewPendingSales,
    rejectSale,
    approveSale,
    itemwise,
    update,
    find
};
