"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sale_repo_1 = __importDefault(require("./sale.repo"));
const shop_services_1 = __importDefault(require("../shop/shop.services"));
const sale_responses_1 = require("./sale.responses");
const mongoose_1 = __importDefault(require("mongoose"));
const sellProduct = async (sale) => {
    const shop = await shop_services_1.default.findById(sale.shopid);
    const product = shop.products.find((p) => p._id == sale.product_id);
    if (!product || product.quantity < sale.quantity) {
        throw sale_responses_1.SALE_RESPONSES.INVALID;
    }
    sale.product_name = product.name;
    sale.Amount = product.price * sale.quantity;
    sale.points = product.Productpoints * sale.quantity;
    product.quantity -= sale.quantity;
    await shop.save();
    const updated = await sale_repo_1.default.create(sale);
    return updated;
};
const findByIdAndUpdate = (filter, update, options = {}) => {
    return sale_repo_1.default.findByIdAndUpdate(filter, update);
};
const find = async (query) => {
    const products = await sale_repo_1.default.find(query);
    return products;
};
const deleteSale = async (saleId) => {
    const sale = await sale_repo_1.default.findById(saleId);
    if (!sale) {
        throw sale_responses_1.SALE_RESPONSES.NOT_FOUND;
    }
    const shop = await shop_services_1.default.findById(sale.shopid);
    const product = shop.products.find((p) => p._id.toString() == sale.product_id);
    if (!product) {
        throw sale_responses_1.SALE_RESPONSES.PRODUCT_NOT_FOUND;
    }
    product.quantity += sale.quantity;
    await shop.save();
    const record = await sale_repo_1.default.update({ _id: new mongoose_1.default.mongo.ObjectId(saleId) }, { isDeleted: true });
    if (!record)
        throw sale_responses_1.SALE_RESPONSES.NOT_FOUND;
    return record;
};
const ViewPendingSales = async () => {
    const Sales = await sale_repo_1.default.ViewPendingSales();
    return Sales;
};
const approveSale = async (shopId) => {
    const { pendingRevenue, saleRecords } = await sale_repo_1.default.ViewPendingSales();
    const salepoints = pendingRevenue[0].totalPoints;
    const { approve, totalRevenue } = await sale_repo_1.default.approveSale();
    const shop = await shop_services_1.default.findOne({ _id: shopId });
    if (shop) {
        shop.points += salepoints;
        shop.totalRevenue += totalRevenue;
        await shop.save();
    }
    else {
        throw sale_responses_1.SALE_RESPONSES.NOT_FOUND;
    }
    return salepoints;
};
const itemwise = async () => {
    sale_repo_1.default.itemwise();
};
const rejectSale = async (shopId) => {
    const shop = await shop_services_1.default.findOne({ _id: shopId });
    const reject = await sale_repo_1.default.rejectSale();
    return reject;
};
exports.default = {
    sellProduct,
    findByIdAndUpdate,
    ViewPendingSales,
    approveSale,
    rejectSale,
    itemwise,
    deleteSale,
    find
};
