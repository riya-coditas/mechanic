"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SALE_RESPONSES = void 0;
exports.SALE_RESPONSES = {
    NOT_FOUND: {
        statusCode: 404,
        message: "Not found",
    },
    INVALID: {
        statusCode: 400,
        message: "Invalid",
    },
    PRODUCT_NOT_FOUND: {
        statusCode: 404,
        message: "Product Not found",
    },
};
