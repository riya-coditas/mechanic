"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SaleModel = void 0;
const mongoose_1 = require("mongoose");
const base_schema_1 = require("../../utils/base-schema");
const SaleSchema = new base_schema_1.BaseSchema({
    shopid: {
        type: mongoose_1.Schema.Types.ObjectId,
        required: true
    },
    product_id: {
        type: mongoose_1.Schema.Types.ObjectId,
        required: true
    },
    product_name: {
        type: String
    },
    quantity: {
        type: Number,
        required: true
    },
    Amount: {
        type: Number
    },
    points: {
        type: Number,
        default: 0
    },
    isValidated: {
        type: Boolean,
        default: false
    },
    isRejected: {
        type: Boolean,
        default: false
    }
});
exports.SaleModel = (0, mongoose_1.model)("Sales", SaleSchema);
