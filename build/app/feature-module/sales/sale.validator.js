"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PARAM_VALIDATION = exports.FINDALL_SALE_VALIDATION = exports.SalesValidator = void 0;
const express_validator_1 = require("express-validator");
const validate_1 = require("../../utils/validate");
exports.SalesValidator = [
    (0, express_validator_1.body)("shopid")
        .isString()
        .withMessage("String value required"),
    (0, express_validator_1.body)("product_id")
        .isString()
        .withMessage("String value required"),
    (0, express_validator_1.body)("product_name")
        .optional()
        .isString()
        .withMessage("String value required"),
    (0, express_validator_1.body)("quantity")
        .isInt()
        .withMessage("Numeric value required"),
    (0, express_validator_1.body)("isApproved")
        .optional()
        .isBoolean()
        .withMessage("Boolean value required"),
    (0, express_validator_1.body)("isRejected")
        .optional()
        .isBoolean()
        .withMessage("Boolean value required"),
    validate_1.validate,
];
exports.FINDALL_SALE_VALIDATION = [
    (0, express_validator_1.query)("sort")
        .optional()
        .isString()
        .withMessage("sort field to sort"),
    (0, express_validator_1.query)("limit")
        .optional()
        .isNumeric()
        .withMessage("limit field to limit"),
    (0, express_validator_1.query)("page")
        .optional()
        .isNumeric()
        .withMessage("page field to paginate"),
    validate_1.validate,
];
exports.PARAM_VALIDATION = [
    (0, express_validator_1.param)("id")
        .isString()
        .withMessage("id parameter is necessary"),
    validate_1.validate,
];
