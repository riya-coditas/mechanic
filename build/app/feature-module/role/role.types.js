"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Roles = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
exports.Roles = {
    admin: new mongoose_1.default.mongo.ObjectId("6445bbca29e6df1c9961d359"),
    owner: new mongoose_1.default.mongo.ObjectId("6445bbd029e6df1c9961d35b"),
};
