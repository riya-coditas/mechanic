"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const role_services_1 = __importDefault(require("./role.services"));
const response_handler_1 = require("../../utils/response_handler");
const permit_1 = require("../../utils/permit");
const role_types_1 = require("./role.types");
const mongoose_1 = __importDefault(require("mongoose"));
const role_validations_1 = require("./role.validations");
const router = (0, express_1.Router)();
router.post("/registerRole", role_validations_1.RoleValidator, async (req, res, next) => {
    try {
        const result = await role_services_1.default.create(req.body);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.get("/", role_validations_1.FINDALL_ROLE_VALIDATION, (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const filter = req.query;
        const result = await role_services_1.default.find(filter);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.patch("/Update/:id", role_validations_1.PARAM_VALIDATION, (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const id = req.params.id;
        const update = req.body;
        const result = await role_services_1.default.findByIdAndUpdate(new mongoose_1.default.mongo.ObjectId(id), update);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.delete("/Delete/:id", role_validations_1.PARAM_VALIDATION, (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const id = req.params.id;
        const result = await role_services_1.default.deleteRole(id);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
exports.default = router;
