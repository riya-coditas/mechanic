"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ROLE_RESPONSE = void 0;
exports.ROLE_RESPONSE = {
    NOT_FOUND: {
        statusCode: 404,
        message: "Role not found",
    },
};
