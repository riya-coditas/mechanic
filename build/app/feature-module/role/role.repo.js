"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const role_schema_1 = require("./role.schema");
const pipeline_1 = require("../../utils/pipeline");
const create = (role) => role_schema_1.RoleModel.create(role);
const find = async (query) => {
    try {
        const pipeline = (0, pipeline_1.generatePipeline)(query);
        return await role_schema_1.RoleModel.aggregate([
            {
                $match: { isDeleted: false },
            },
            ...pipeline,
        ]);
    }
    catch (err) {
        throw { message: "something went wrong", e: err };
    }
};
const findByIdAndUpdate = (filter, update, options = {}) => {
    return role_schema_1.RoleModel.findByIdAndUpdate(filter, update, Object.assign(Object.assign({}, options), { new: true }));
};
const update = async (filter, update) => await role_schema_1.RoleModel.updateOne(filter, update);
exports.default = {
    create,
    find,
    findByIdAndUpdate,
    update
};
