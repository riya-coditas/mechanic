"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const role_repo_1 = __importDefault(require("./role.repo"));
const role_response_1 = require("./role.response");
const create = (role) => role_repo_1.default.create(role);
const find = async (query) => {
    const products = await role_repo_1.default.find(query);
    return products;
};
const findByIdAndUpdate = (filter, update, options = {}) => {
    return role_repo_1.default.findByIdAndUpdate(filter, update, Object.assign(Object.assign({}, options), { new: true }));
};
const deleteRole = async (id) => {
    const record = await role_repo_1.default.update({ _id: new mongoose_1.default.mongo.ObjectId(id) }, { isDeleted: true });
    if (!record)
        throw role_response_1.ROLE_RESPONSE.NOT_FOUND;
    return record;
};
exports.default = {
    create,
    find,
    findByIdAndUpdate,
    deleteRole
};
