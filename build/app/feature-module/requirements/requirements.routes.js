"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const requirements_services_1 = __importDefault(require("./requirements.services"));
const response_handler_1 = require("../../utils/response_handler");
const role_types_1 = require("../role/role.types");
const mongoose_1 = __importDefault(require("mongoose"));
const requirement_validations_1 = require("./requirement.validations");
const permit_1 = require("../../utils/permit");
const router = (0, express_1.Router)();
router.get("/", requirement_validations_1.FINDALL_REQUIREMENT_VALIDATION, (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const filter = req.query;
        const result = await requirements_services_1.default.find(filter);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.post("/AddRequirements/:id", requirement_validations_1.PARAM_VALIDATION, requirement_validations_1.RequirementValidator, (0, permit_1.permit)([role_types_1.Roles.owner.toString()]), async (req, res, next) => {
    try {
        const id = req.params.id;
        const requirements = req.body;
        const result = await requirements_services_1.default.create(requirements, id);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.post("/approve/:id", requirement_validations_1.PARAM_VALIDATION, (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const requirementId = req.params.id;
        const result = await requirements_services_1.default.approveRequirement(requirementId);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.patch("/Update/:id", requirement_validations_1.PARAM_VALIDATION, (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const id = req.params.id;
        const update = req.body;
        const result = await requirements_services_1.default.findByIdAndUpdate(new mongoose_1.default.mongo.ObjectId(id), update);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.delete("/Delete/:id", requirement_validations_1.PARAM_VALIDATION, (0, permit_1.permit)([role_types_1.Roles.owner.toString()]), async (req, res, next) => {
    try {
        const id = req.params.id;
        const result = await requirements_services_1.default.deleteRequirement(id);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
exports.default = router;
