"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RequirementModel = void 0;
const mongoose_1 = require("mongoose");
const base_schema_1 = require("../../utils/base-schema");
const RequirementSchema = new base_schema_1.BaseSchema({
    shop_id: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Shops',
        required: true
    },
    product_id: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: "Product",
        required: true
    },
    quantity: {
        type: Number,
        required: true
    },
    isApproved: {
        type: Boolean,
        default: false
    }
});
exports.RequirementModel = (0, mongoose_1.model)("Requirements", RequirementSchema);
