"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const shop_services_1 = __importDefault(require("../shop/shop.services"));
const requirements_repo_1 = __importDefault(require("./requirements.repo"));
const requirements_responses_1 = require("./requirements.responses");
const inventory_schema_1 = require("../Inventory/inventory.schema");
const create = async (order, id) => {
    const shop = await shop_services_1.default.findOne({ _id: id });
    order.shop_id = shop.id;
    const orderplaced = await requirements_repo_1.default.create(order);
    return orderplaced;
};
const findByIdAndUpdate = (filter, update, options = {}) => {
    return requirements_repo_1.default.findByIdAndUpdate(filter, update);
};
const find = async (query) => {
    const products = await requirements_repo_1.default.find(query);
    return products;
};
const deleteRequirement = async (id) => {
    const record = await requirements_repo_1.default.update({ _id: new mongoose_1.default.mongo.ObjectId(id) }, { isDeleted: true });
    if (!record)
        throw requirements_responses_1.REQUIREMENT_RESPONSES.NOT_FOUND;
    return record;
};
const approveRequirement = async (requirementId) => {
    const requirement = await requirements_repo_1.default.findById(requirementId);
    if (!requirement) {
        throw requirements_responses_1.REQUIREMENT_RESPONSES.NOT_FOUND;
    }
    const product = await inventory_schema_1.ProductModel.findById(requirement.product_id);
    const shop = await shop_services_1.default.findById(requirement.shop_id);
    if (!product || !shop || product.quantity < requirement.quantity) {
        throw requirements_responses_1.REQUIREMENT_RESPONSES.INVALID;
    }
    let existingProduct = shop.products.find((p) => p._id.toString() == product._id.toString());
    if (existingProduct) {
        existingProduct.quantity += requirement.quantity;
    }
    else {
        shop.products.push({
            _id: product._id,
            name: product.name,
            price: product.price,
            quantity: requirement.quantity,
            Threshold: product.Threshold,
            Productpoints: product.Productpoints
        });
    }
    shop.save();
    product.quantity -= requirement.quantity;
    await product.save();
    const approve = await requirements_repo_1.default.findByIdAndUpdate({ _id: new mongoose_1.default.mongo.ObjectId(requirementId) }, { isApproved: true }, { new: true });
    await requirement.save();
    return approve;
};
exports.default = {
    create,
    approveRequirement,
    findByIdAndUpdate,
    find,
    deleteRequirement
};
