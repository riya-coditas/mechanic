"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const requirements_schema_1 = require("./requirements.schema");
const pipeline_1 = require("../../utils/pipeline");
const create = (requirement) => requirements_schema_1.RequirementModel.create(requirement);
const findByIdAndUpdate = (filter, update, options = {}) => {
    return requirements_schema_1.RequirementModel.findByIdAndUpdate(filter, update, Object.assign(Object.assign({}, options), { new: true }));
};
const findById = async (id) => {
    const requirement = await requirements_schema_1.RequirementModel.findById(id).exec();
    return requirement;
};
const find = async (query) => {
    try {
        const pipeline = (0, pipeline_1.generatePipeline)(query);
        return await requirements_schema_1.RequirementModel.aggregate([
            {
                $match: { isDeleted: false },
            },
            ...pipeline,
        ]);
    }
    catch (err) {
        throw { message: "something went wrong", e: err };
    }
};
const update = async (filter, update) => await requirements_schema_1.RequirementModel.updateOne(filter, update);
exports.default = {
    create,
    findByIdAndUpdate,
    findById,
    find,
    update
};
