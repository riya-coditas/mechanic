"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.REQUIREMENT_RESPONSES = void 0;
exports.REQUIREMENT_RESPONSES = {
    NOT_FOUND: {
        statusCode: 404,
        message: "Requirement Not Found"
    },
    INVALID: {
        statusCode: 404,
        message: "Product not available in inventory or quantity not enough"
    },
};
