"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcryptjs_1 = require("bcryptjs");
const user_services_1 = __importDefault(require("../user/user.services"));
const auth_responses_1 = require("./auth.responses");
const role_types_1 = require("../role/role.types");
const jsonwebtoken_1 = __importStar(require("jsonwebtoken"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const encryptUserPassword = async (user) => {
    const salt = await (0, bcryptjs_1.genSalt)(10);
    const hashedPassword = await (0, bcryptjs_1.hash)(user.password, salt);
    user.password = hashedPassword;
    return user;
};
const register = async (user) => {
    user = await encryptUserPassword(user);
    if (!user.role) {
        user.role = role_types_1.Roles.owner;
    }
    const record = user_services_1.default.create(user);
    return record;
};
const login = async (credentials) => {
    const user = await user_services_1.default.findOne({ email: credentials.email });
    if (!user)
        throw auth_responses_1.AUTH_RESPONSES.INVALID_CREDENTIALS;
    const isPasswordValid = await (0, bcryptjs_1.compare)(credentials.password, user.password);
    if (!isPasswordValid)
        throw auth_responses_1.AUTH_RESPONSES.INVALID_CREDENTIALS;
    const { _id, role } = user;
    const PRIVATE_KEY = fs_1.default.readFileSync(path_1.default.resolve(__dirname, "..\\..\\keys\\private.pem"), { encoding: "utf-8" });
    try {
        var token = jsonwebtoken_1.default.sign({ id: _id, role: role }, PRIVATE_KEY || "", {
            algorithm: "RS256", expiresIn: '9000s'
        });
        var refreshToken = jsonwebtoken_1.default.sign({ id: _id, role: role }, PRIVATE_KEY || "", {
            algorithm: "RS256", expiresIn: '9000s',
        });
        return { token, role, refreshToken };
    }
    catch (error) {
        console.log(error);
    }
};
//  const {JWT_SECRET} = process.env;
//  const {_id,role} = user;
//  const token = sign({id:_id,role:role},JWT_SECRET||"");
//  return {token,role};
const refreshToken = (token) => {
    const PUBLIC_KEY = fs_1.default.readFileSync(path_1.default.resolve(__dirname, "..\\..\\keys\\public.pem"), { encoding: "utf-8" });
    const tokenDecode = (0, jsonwebtoken_1.verify)(token || "", PUBLIC_KEY || "");
    console.log(tokenDecode);
    const PRIVATE_KEY = fs_1.default.readFileSync(path_1.default.resolve(__dirname, "..\\..\\keys\\private.pem"), { encoding: "utf-8" });
    if (tokenDecode) {
        const accessToken = jsonwebtoken_1.default.sign({ id: tokenDecode.id, role: tokenDecode.role }, PRIVATE_KEY || "", { algorithm: "RS256", expiresIn: "9000s" });
        return { accessToken };
    }
    else {
        throw { statusCode: 400, message: "Token invalid" };
    }
};
exports.default = {
    register,
    login,
    refreshToken
};
