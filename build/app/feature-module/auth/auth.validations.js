"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoginValidator = void 0;
const express_validator_1 = require("express-validator");
const validate_1 = require("../../utils/validate");
exports.LoginValidator = [
    (0, express_validator_1.body)("email")
        .isEmail()
        .isLength({ min: 6 })
        .withMessage("Email is Required"),
    (0, express_validator_1.body)("password")
        .isString()
        .isLength({ min: 6 })
        .withMessage("Password is Required"),
    validate_1.validate,
];
