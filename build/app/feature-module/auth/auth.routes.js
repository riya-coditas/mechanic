"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const response_handler_1 = require("../../utils/response_handler");
const auth_services_1 = __importDefault(require("./auth.services"));
const auth_validations_1 = require("./auth.validations");
const router = (0, express_1.Router)();
router.post("/register", auth_validations_1.LoginValidator, async (req, res, next) => {
    try {
        const user = req.body;
        const result = await auth_services_1.default.register(user);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.post("/login", auth_validations_1.LoginValidator, async (req, res, next) => {
    try {
        const credentials = req.body;
        const result = await auth_services_1.default.login(credentials);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
    next();
});
router.post("/refresh", async (req, res, next) => {
    try {
        const refreshToken = req.body.refreshToken;
        const result = auth_services_1.default.refreshToken(refreshToken);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
    next();
});
exports.default = router;
