"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FINDALL_USER_VALIDATION = exports.PARAM_VALIDATION = void 0;
const express_validator_1 = require("express-validator");
const validate_1 = require("../../utils/validate");
exports.PARAM_VALIDATION = [
    (0, express_validator_1.param)("id")
        .isString()
        .withMessage("id parameter is necessary"),
    validate_1.validate,
];
exports.FINDALL_USER_VALIDATION = [
    (0, express_validator_1.query)("sort")
        .optional()
        .isString()
        .withMessage("sort field to sort"),
    (0, express_validator_1.query)("limit")
        .optional()
        .isNumeric()
        .withMessage("limit field to limit"),
    (0, express_validator_1.query)("page")
        .optional()
        .isNumeric()
        .withMessage("page field to paginate"),
    validate_1.validate,
];
