"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_schema_1 = require("./user.schema");
const create = (user) => user_schema_1.UserModel.create(user);
const find = async () => {
    try {
        return await user_schema_1.UserModel.find({
            isDeleted: false,
        });
    }
    catch (err) {
        throw { message: "something went wrong", e: err };
    }
};
const findOne = async (filters) => {
    try {
        return await user_schema_1.UserModel.findOne(Object.assign(Object.assign({}, filters), { isDeleted: false }));
    }
    catch (err) {
        throw { message: 'something went wrong', e: err };
    }
};
const findByIdAndUpdate = (filter, update, options = {}) => {
    return user_schema_1.UserModel.findByIdAndUpdate(filter, update, Object.assign(Object.assign({}, options), { new: true }));
};
const update = async (filter, update) => await user_schema_1.UserModel.updateOne(filter, update);
exports.default = {
    create,
    find,
    findOne,
    findByIdAndUpdate,
    update
};
