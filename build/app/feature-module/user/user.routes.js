"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const response_handler_1 = require("../../utils/response_handler");
const role_types_1 = require("../role/role.types");
const user_services_1 = __importDefault(require("./user.services"));
const mongoose_1 = __importDefault(require("mongoose"));
const permit_1 = require("../../utils/permit");
const user_validations_1 = require("./user.validations");
const router = (0, express_1.Router)();
router.get("/", user_validations_1.FINDALL_USER_VALIDATION, (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const result = await user_services_1.default.find();
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.patch("/Update/:id", user_validations_1.PARAM_VALIDATION, (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const id = req.params.id;
        const update = req.body;
        const result = await user_services_1.default.findByIdAndUpdate(new mongoose_1.default.mongo.ObjectId(id), update);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.delete("/Delete/:id", user_validations_1.PARAM_VALIDATION, (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const id = req.params.id;
        const result = await user_services_1.default.deleteUser(id);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
exports.default = router;
