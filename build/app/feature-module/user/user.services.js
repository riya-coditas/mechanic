"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const user_repo_1 = __importDefault(require("./user.repo"));
const user_responses_1 = require("./user.responses");
const create = (user) => user_repo_1.default.create(user);
const find = () => user_repo_1.default.find();
const findOne = async (filters) => {
    const user = await user_repo_1.default.findOne(filters);
    if (!user)
        throw user_responses_1.USER_RESPONSE.NOT_FOUND;
    return user;
};
const findByIdAndUpdate = (filter, update, options = {}) => {
    return user_repo_1.default.findByIdAndUpdate(filter, update, Object.assign(Object.assign({}, options), { new: true }));
};
const deleteUser = async (id) => {
    const record = await user_repo_1.default.update({ _id: new mongoose_1.default.mongo.ObjectId(id) }, { isDeleted: true });
    if (!record)
        throw user_responses_1.USER_RESPONSE.NOT_FOUND;
    return record;
};
exports.default = {
    create,
    find,
    findOne,
    findByIdAndUpdate,
    deleteUser
};
