"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.USER_RESPONSE = void 0;
exports.USER_RESPONSE = {
    NOT_FOUND: {
        statusCode: 404,
        message: 'user not found'
    }
};
