"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const routes_data_1 = require("../../routes/routes.data");
const authorize_1 = require("../../utils/authorize");
const response_handler_1 = require("../../utils/response_handler");
const restaurant_services_1 = __importDefault(require("./restaurant.services"));
const router = (0, express_1.Router)();
router.post('/RegisterRestaurant', (0, authorize_1.authorize)(routes_data_1.excludedPaths), (0, authorize_1.validateRole)(['64142d58c9de184b14f63474']), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const restaurantData = req.body;
        const result = yield restaurant_services_1.default.RegisterRestaurant(restaurantData);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
router.post('/ApproveRestaurant/:id', (0, authorize_1.authorize)(routes_data_1.excludedPaths), (0, authorize_1.validateRole)(["64142d25c9de184b14f63470"]), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const restaurantId = req.params.id;
        const restaurantCredentials = { _id: restaurantId };
        const result = yield restaurant_services_1.default.ApproveRestaurant(restaurantCredentials);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
router.post('/RejectRestaurant/:id', (0, authorize_1.authorize)(routes_data_1.excludedPaths), (0, authorize_1.validateRole)(["64142d25c9de184b14f63470"]), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const restaurantId = req.params.id;
        const restaurantCredentials = { _id: restaurantId };
        const result = yield restaurant_services_1.default.RejectRestaurant(restaurantCredentials);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
router.post('/UpdateRestaurant/:id', (0, authorize_1.authorize)(routes_data_1.excludedPaths), (0, authorize_1.validateRole)(["64142d54c9de184b14f63472"]), (0, authorize_1.checkRestaurantOwner)(), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const id = req.params.id;
        const update = req.body;
        const result = yield restaurant_services_1.default.updateMyRestaurant(id, update);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
exports.default = router;
