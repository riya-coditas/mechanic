"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const restaurant_repo_1 = __importDefault(require("./restaurant.repo"));
const restaurant_responses_1 = require("./restaurant-responses");
const user_services_1 = __importDefault(require("../user/user.services"));
const role_types_1 = require("../role/role.types");
const create = (restaurant) => restaurant_repo_1.default.create(restaurant);
const findByIdAndUpdate = (filter, update, options = {}) => {
    return restaurant_repo_1.default.findByIdAndUpdate(filter, update);
};
const find = () => __awaiter(void 0, void 0, void 0, function* () {
    const list = yield restaurant_repo_1.default.find();
    if (!list)
        throw restaurant_responses_1.Restaurant_response.Restaurant_not_found;
    return list;
});
const findOne = (filters) => __awaiter(void 0, void 0, void 0, function* () {
    const restaurant = yield restaurant_repo_1.default.findOne(filters);
    if (!restaurant)
        throw restaurant_responses_1.Restaurant_response.Restaurant_not_found;
    return restaurant;
});
const RegisterRestaurant = (data) => __awaiter(void 0, void 0, void 0, function* () {
    const restaurant = yield restaurant_repo_1.default.create(data);
    const userId = data.owner_id;
    const updatedUser = yield user_services_1.default.findByIdAndUpdate(userId, { role: role_types_1.Roles.owner });
    console.log(restaurant);
    return restaurant;
});
const ApproveRestaurant = (id) => __awaiter(void 0, void 0, void 0, function* () {
    const filter = { _id: id._id };
    const update = { isApproved: true, isRejected: false };
    const restaurant = yield restaurant_repo_1.default.findByIdAndUpdate(filter, update);
    console.log(filter, update);
    return restaurant;
});
const RejectRestaurant = (id) => __awaiter(void 0, void 0, void 0, function* () {
    const filter = { _id: id._id };
    const update = { isApproved: false, isRejected: true };
    const restaurant = yield restaurant_repo_1.default.findByIdAndUpdate(filter, update);
    console.log(filter, update);
    return restaurant;
});
const updateMyRestaurant = (restaurantId, update) => __awaiter(void 0, void 0, void 0, function* () {
    const updatedtResponse = yield restaurant_repo_1.default.findByIdAndUpdate({ _id: restaurantId }, update);
    return updatedtResponse;
});
exports.default = {
    create,
    findByIdAndUpdate,
    find,
    findOne,
    RegisterRestaurant,
    ApproveRestaurant,
    RejectRestaurant,
    updateMyRestaurant
};
