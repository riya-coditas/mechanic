"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sale_responses_1 = require("../sales/sale.responses");
const shop_services_1 = __importDefault(require("../shop/shop.services"));
const sellProduct = (sale) => __awaiter(void 0, void 0, void 0, function* () {
    const shop = yield shop_services_1.default.findById(sale.shopid);
    //console.log(shop)
    const product = shop.products.find((p) => p._id == sale.product_id);
    if (!product || product.quantity < sale.quantity) {
        sale_responses_1.SALE_RESPONSES.INVALID;
    }
    sale.Amount = product.price * sale.quantity;
    const shopId = sale.shopid;
    const update = {
        $inc: {
        //"products.$.quantity": -sale.quantity,
        //points: product!.Productpoints * sale.quantity
        },
        $pull: { products: { quantity: 0 } }
    };
    const updatedshop = yield shop_services_1.default.findByIdAndUpdate(shopId, update);
    return updatedshop;
});
exports.default = {
    sellProduct,
};
