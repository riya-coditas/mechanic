"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const auth_routes_1 = __importDefault(require("../feature-module/auth/auth.routes"));
const user_routes_1 = __importDefault(require("../feature-module/user/user.routes"));
const role_routes_1 = __importDefault(require("../feature-module/role/role.routes"));
const inventory_routes_1 = __importDefault(require("../feature-module/Inventory/inventory.routes"));
const shop_routes_1 = __importDefault(require("../feature-module/shop/shop.routes"));
const requirements_routes_1 = __importDefault(require("../feature-module/requirements/requirements.routes"));
const sale_routes_1 = __importDefault(require("../feature-module/sales/sale.routes"));
const reward_routes_1 = __importDefault(require("../feature-module/rewards/reward.routes"));
exports.default = {
    roleRouter: role_routes_1.default,
    authRouter: auth_routes_1.default,
    userRouter: user_routes_1.default,
    productRouter: inventory_routes_1.default,
    shopRouter: shop_routes_1.default,
    requirementRouter: requirements_routes_1.default,
    saleRouter: sale_routes_1.default,
    rewardRouter: reward_routes_1.default
};
