"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RewardModel = exports.RewardSchema = void 0;
const mongoose_1 = require("mongoose");
const base_schema_1 = require("../../utils/base-schema");
exports.RewardSchema = new base_schema_1.BaseSchema({
    name: {
        type: String,
        required: true
    },
    points: {
        type: Number,
        required: true
    }
});
exports.RewardModel = (0, mongoose_1.model)("Reward", exports.RewardSchema);
