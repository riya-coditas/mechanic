"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PARAM_VALIDATION = exports.RewardValidator = void 0;
const express_validator_1 = require("express-validator");
const validate_1 = require("../../utils/validate");
exports.RewardValidator = [
    (0, express_validator_1.body)("name")
        .isString()
        .withMessage("String value required"),
    (0, express_validator_1.body)("points")
        .isInt()
        .withMessage("Numeric value required"),
    validate_1.validate,
];
exports.PARAM_VALIDATION = [
    (0, express_validator_1.param)("id")
        .isString()
        .withMessage("id parameter is necessary"),
    validate_1.validate,
];
