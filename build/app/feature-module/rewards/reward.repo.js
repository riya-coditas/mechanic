"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const reward_schema_1 = require("./reward.schema");
const pipeline_1 = require("../../utils/pipeline");
const shop_services_1 = __importDefault(require("../shop/shop.services"));
const reward_reponses_1 = require("./reward.reponses");
const create = (reward) => reward_schema_1.RewardModel.create(reward);
const findById = async (id) => {
    const reward = await reward_schema_1.RewardModel.findById(id);
    return reward;
};
const findByIdAndUpdate = (filter, update, options = {}) => {
    return reward_schema_1.RewardModel.findByIdAndUpdate(filter, update, Object.assign(Object.assign({}, options), { new: true }));
};
const findOne = async (filters) => {
    try {
        return await reward_schema_1.RewardModel.findOne(Object.assign(Object.assign({}, filters), { isDeleted: false }));
    }
    catch (err) {
        throw { message: "something went wrong", e: err };
    }
};
const find = async (query) => {
    try {
        const pipeline = (0, pipeline_1.generatePipeline)(query);
        return await reward_schema_1.RewardModel.aggregate([
            {
                $match: { isDeleted: false },
            },
            ...pipeline,
        ]);
    }
    catch (err) {
        throw { message: "something went wrong", e: err };
    }
};
const update = async (filter, update) => await reward_schema_1.RewardModel.updateOne(filter, update);
const findUpcomingReward = async (id) => {
    const shop = await shop_services_1.default.findById(id);
    if (!shop) {
        throw reward_reponses_1.REWARD_RESPONSES.SHOP_NOT_FOUND;
    }
    const upcomingReward = await reward_schema_1.RewardModel.aggregate([
        {
            $match: { points: { $gt: shop.points } },
        },
        {
            $limit: 1,
        },
    ]);
    if (!shop.points) {
        throw reward_reponses_1.REWARD_RESPONSES.SHOP_NOT_FOUND;
    }
    const pointsLeft = upcomingReward[0].points - shop.points;
    return [upcomingReward[0], "You have " + pointsLeft + " points left to redem the next Reward"];
};
exports.default = {
    create,
    findById,
    findOne,
    find,
    findByIdAndUpdate,
    update,
    findUpcomingReward
};
