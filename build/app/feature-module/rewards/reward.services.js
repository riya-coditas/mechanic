"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const reward_repo_1 = __importDefault(require("./reward.repo"));
const reward_reponses_1 = require("./reward.reponses");
const create = (reward) => reward_repo_1.default.create(reward);
const findById = async (id) => {
    const reward = await reward_repo_1.default.findById(id);
    return reward;
};
const findByIdAndUpdate = (filter, update, options = {}) => {
    return reward_repo_1.default.findByIdAndUpdate(filter, update);
};
const findOne = async (filters) => {
    const reward = await reward_repo_1.default.findOne(filters);
    if (!reward)
        throw reward_reponses_1.REWARD_RESPONSES.NOT_FOUND;
    return reward;
};
const find = async (query) => {
    const rewards = await reward_repo_1.default.find(query);
    return rewards;
};
const deleteReward = async (id) => {
    const record = await reward_repo_1.default.update({ _id: new mongoose_1.default.mongo.ObjectId(id) }, { isDeleted: true });
    if (!record)
        throw reward_reponses_1.REWARD_RESPONSES.NOT_FOUND;
    return record;
};
const findUpcomingReward = async (id) => {
    const upcomingReward = reward_repo_1.default.findUpcomingReward(id);
    return upcomingReward;
};
exports.default = {
    create,
    findById,
    findOne,
    find,
    findByIdAndUpdate,
    deleteReward,
    findUpcomingReward
};
