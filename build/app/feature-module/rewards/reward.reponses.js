"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.REWARD_RESPONSES = void 0;
exports.REWARD_RESPONSES = {
    NOT_FOUND: {
        statusCode: 404,
        message: "Reward Not Found",
    },
    SHOP_NOT_FOUND: {
        statusCode: 404,
        message: "Shop Not Found",
    },
};
