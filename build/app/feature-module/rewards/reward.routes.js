"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const response_handler_1 = require("../../utils/response_handler");
const reward_services_1 = __importDefault(require("./reward.services"));
const role_types_1 = require("../role/role.types");
const mongoose_1 = __importDefault(require("mongoose"));
const permit_1 = require("../../utils/permit");
const reward_validations_1 = require("./reward.validations");
const router = (0, express_1.Router)();
router.get("/", (0, permit_1.permit)([role_types_1.Roles.admin.toString(), role_types_1.Roles.owner.toString()]), async (req, res, next) => {
    try {
        const filter = req.query;
        const result = await reward_services_1.default.find(filter);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.post("/create", reward_validations_1.RewardValidator, (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const result = await reward_services_1.default.create(req.body);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.patch("/Update/:id", reward_validations_1.PARAM_VALIDATION, (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const id = req.params.id;
        const update = req.body;
        const result = await reward_services_1.default.findByIdAndUpdate(new mongoose_1.default.mongo.ObjectId(id), update);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.delete("/Delete/:id", reward_validations_1.PARAM_VALIDATION, (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const id = req.params.id;
        const result = await reward_services_1.default.deleteReward(id);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.get("/UpcomingReward/:id", reward_validations_1.PARAM_VALIDATION, (0, permit_1.permit)([role_types_1.Roles.owner.toString()]), async (req, res, next) => {
    try {
        const id = req.params.id;
        const result = await reward_services_1.default.findUpcomingReward(id);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
exports.default = router;
