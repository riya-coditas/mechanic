"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductModel = exports.ProductSchema = void 0;
const mongoose_1 = require("mongoose");
const base_schema_1 = require("../../utils/base-schema");
exports.ProductSchema = new base_schema_1.BaseSchema({
    name: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    quantity: {
        type: Number,
        required: true
    },
    Threshold: {
        type: Number,
    },
    Productpoints: {
        type: Number,
        default: 0
    }
});
exports.ProductModel = (0, mongoose_1.model)("Product", exports.ProductSchema);
