"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PRODUCT_RESPONSES = void 0;
exports.PRODUCT_RESPONSES = {
    NOT_FOUND: {
        statusCode: 404,
        message: "Product Not Found"
    }
};
