"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const inventory_repo_1 = __importDefault(require("./inventory.repo"));
const inventory_responses_1 = require("./inventory.responses");
const create = (product) => inventory_repo_1.default.create(product);
const findOne = async (filters) => {
    const product = await inventory_repo_1.default.findOne(filters);
    if (!product)
        throw "Not found";
    return product;
};
const findByIdAndUpdate = (filter, update, options = {}) => {
    return inventory_repo_1.default.findByIdAndUpdate(filter, update);
};
const find = async (query) => {
    const products = await inventory_repo_1.default.find(query);
    return products;
};
const deleteProduct = async (id) => {
    const record = await inventory_repo_1.default.update({ _id: new mongoose_1.default.mongo.ObjectId(id) }, { isDeleted: true });
    if (!record)
        throw inventory_responses_1.PRODUCT_RESPONSES.NOT_FOUND;
    return record;
};
exports.default = {
    create,
    findOne,
    find,
    findByIdAndUpdate,
    deleteProduct,
};
