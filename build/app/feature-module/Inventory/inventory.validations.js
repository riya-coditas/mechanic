"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PARAM_VALIDATION = exports.FINDALL_PRODUCT_VALIDATION = exports.ProductValidator = void 0;
const express_validator_1 = require("express-validator");
const validate_1 = require("../../utils/validate");
exports.ProductValidator = [
    (0, express_validator_1.body)("name")
        .isString()
        .withMessage("String value required"),
    (0, express_validator_1.body)("price")
        .isInt()
        .withMessage("Numeric value required"),
    (0, express_validator_1.body)("quantity")
        .isInt()
        .withMessage("Numeric value required"),
    (0, express_validator_1.body)("Productpoints").optional(),
    (0, express_validator_1.body)("Threshold")
        .isInt()
        .withMessage("Numeric value required"),
    validate_1.validate,
];
exports.FINDALL_PRODUCT_VALIDATION = [
    (0, express_validator_1.query)("sort")
        .optional()
        .isString()
        .withMessage("sort field to sort"),
    (0, express_validator_1.query)("limit")
        .optional()
        .isNumeric()
        .withMessage("limit field to limit"),
    (0, express_validator_1.query)("page")
        .optional()
        .isNumeric()
        .withMessage("page field to paginate"),
    validate_1.validate,
];
exports.PARAM_VALIDATION = [
    (0, express_validator_1.param)("id")
        .isString()
        .withMessage("id parameter is necessary"),
    validate_1.validate,
];
