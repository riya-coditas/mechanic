"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const response_handler_1 = require("../../utils/response_handler");
const inventory_services_1 = __importDefault(require("./inventory.services"));
const role_types_1 = require("../role/role.types");
const mongoose_1 = __importDefault(require("mongoose"));
const inventory_validations_1 = require("./inventory.validations");
const permit_1 = require("../../utils/permit");
const router = (0, express_1.Router)();
router.get("/", inventory_validations_1.FINDALL_PRODUCT_VALIDATION, (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const filter = req.query;
        const result = await inventory_services_1.default.find(filter);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.post("/createProduct", (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), inventory_validations_1.ProductValidator, async (req, res, next) => {
    try {
        const result = await inventory_services_1.default.create(req.body);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.patch("/Update/:id", inventory_validations_1.PARAM_VALIDATION, (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const id = req.params.id;
        const update = req.body;
        const result = await inventory_services_1.default.findByIdAndUpdate(new mongoose_1.default.mongo.ObjectId(id), update);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.delete("/Delete/:id", inventory_validations_1.PARAM_VALIDATION, (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const id = req.params.id;
        const result = await inventory_services_1.default.deleteProduct(id);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
exports.default = router;
