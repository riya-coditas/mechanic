"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const inventory_schema_1 = require("./inventory.schema");
const pipeline_1 = require("../../utils/pipeline");
const create = (product) => inventory_schema_1.ProductModel.create(product);
const findOne = async (filters) => {
    try {
        return await inventory_schema_1.ProductModel.findOne(Object.assign(Object.assign({}, filters), { isDeleted: false }));
    }
    catch (err) {
        throw { message: "something went wrong", e: err };
    }
};
const findByIdAndUpdate = (filter, update, options = {}) => {
    return inventory_schema_1.ProductModel.findByIdAndUpdate(filter, update, Object.assign(Object.assign({}, options), { new: true }));
};
const find = async (query) => {
    try {
        const pipeline = (0, pipeline_1.generatePipeline)(query);
        return await inventory_schema_1.ProductModel.aggregate([
            {
                $match: { isDeleted: false },
            },
            ...pipeline,
        ]);
    }
    catch (err) {
        throw { message: "something went wrong", e: err };
    }
};
const update = async (filter, update) => await inventory_schema_1.ProductModel.updateOne(filter, update);
exports.default = {
    create,
    findOne,
    find,
    update,
    findByIdAndUpdate
};
