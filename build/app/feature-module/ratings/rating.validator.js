"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RatingValidator = void 0;
const express_validator_1 = require("express-validator");
const validate_1 = require("../../utils/validate");
exports.RatingValidator = [
    (0, express_validator_1.body)("ratings").isInt().withMessage("Numeric value required"),
    validate_1.validate,
];
