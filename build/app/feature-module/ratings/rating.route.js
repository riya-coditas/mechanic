"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const response_handler_1 = require("../../utils/response_handler");
const rating_services_1 = __importDefault(require("./rating.services"));
const role_types_1 = require("../role/role.types");
const rating_validator_1 = require("./rating.validator");
const rating_repo_1 = __importDefault(require("./rating.repo"));
const permit_1 = require("../../utils/permit");
const router = (0, express_1.Router)();
router.post("/rating", rating_validator_1.RatingValidator, async (req, res, next) => {
    try {
        const result = await rating_services_1.default.create(req.body);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.post("/Averagerating", (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const shop_id = req.body;
        const result = await rating_repo_1.default.ShopRating(shop_id);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
exports.default = router;
