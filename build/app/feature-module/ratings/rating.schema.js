"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RatingModel = exports.RatingSchema = void 0;
const mongoose_1 = require("mongoose");
const base_schema_1 = require("../../utils/base-schema");
exports.RatingSchema = new base_schema_1.BaseSchema({
    shop_id: {
        type: String,
        ref: 'Shops'
    },
    ratings: {
        type: Number,
        required: true,
        min: 1,
        max: 5
    }
});
exports.RatingModel = (0, mongoose_1.model)("Ratings", exports.RatingSchema);
