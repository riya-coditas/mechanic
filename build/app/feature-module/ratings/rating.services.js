"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const shop_services_1 = __importDefault(require("../shop/shop.services"));
const rating_repo_1 = __importDefault(require("./rating.repo"));
const rating_schema_1 = require("./rating.schema");
const create = async (rating) => {
    const { shop_id, ratings } = rating;
    const shop = await shop_services_1.default.findById(shop_id);
    const result = rating_schema_1.RatingModel.create(rating);
    return result;
};
const shopRating = async (shop_id) => {
    const result = await rating_repo_1.default.ShopRating(shop_id);
    console.log(result);
    const avgRating = result[0].avgRating;
    const shop = await shop_services_1.default.findById(shop_id);
    //shop.rating = avgRating
    await shop.save();
    return shop;
};
exports.default = {
    create,
    shopRating
};
