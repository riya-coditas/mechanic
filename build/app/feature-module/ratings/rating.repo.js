"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rating_schema_1 = require("./rating.schema");
const create = (rating) => rating_schema_1.RatingModel.create(rating);
const ShopRating = async (shop_id) => {
    const ratings = await rating_schema_1.RatingModel.aggregate([
        { $match: { shop_id } },
        {
            $group: {
                _id: '$shop_id',
                avgRating: { $avg: '$ratings' }
            }
        }
    ]);
    return ratings;
};
exports.default = {
    create,
    ShopRating
};
