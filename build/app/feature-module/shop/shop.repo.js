"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const shop_schema_1 = require("./shop.schema");
const mongoose_1 = __importDefault(require("mongoose"));
const shop_responses_1 = require("./shop.responses");
const pipeline_1 = require("../../utils/pipeline");
const create = (shop) => shop_schema_1.ShopModel.create(shop);
const findOne = async (filters) => {
    try {
        return await shop_schema_1.ShopModel.findOne(Object.assign(Object.assign({}, filters), { isDeleted: false }));
    }
    catch (err) {
        throw { message: 'something went wrong', e: err };
    }
};
const findById = async (id) => {
    const shop = await shop_schema_1.ShopModel.findById(id);
    if (!shop) {
        throw shop_responses_1.SHOP_RESPONSES.NOT_FOUND;
    }
    return shop;
};
const find = async (query) => {
    const pipeline = (0, pipeline_1.generatePipeline)(query);
    return await shop_schema_1.ShopModel.aggregate([
        { $match: { isDeleted: false } },
        { $project: { owner: 1, address: 1, rating: 1 } },
        ...pipeline,
    ]);
};
const findByIdAndUpdate = (filter, update, options = {}) => {
    return shop_schema_1.ShopModel.findByIdAndUpdate(filter, update, Object.assign(Object.assign({}, options), { new: true }));
};
const update = async (filter, update) => await shop_schema_1.ShopModel.updateOne(filter, update);
const findAll = async (query) => {
    try {
        const pipeline = (0, pipeline_1.generatePipeline)(query);
        return await shop_schema_1.ShopModel.aggregate([
            {
                $match: { isDeleted: false },
            },
            ...pipeline,
        ]);
    }
    catch (err) {
        throw { message: "something went wrong", e: err };
    }
};
const RevenueWise = async () => await shop_schema_1.ShopModel.find().sort({ totalRevenue: -1 });
const PointWise = async () => await shop_schema_1.ShopModel.find().sort({ points: -1 });
const addShopRating = async (shopId, ratings) => {
    await shop_schema_1.ShopModel.findByIdAndUpdate(shopId, { $push: { ratings: ratings } });
};
const findLowProducts = async () => {
    try {
        const products = await shop_schema_1.ShopModel.aggregate([
            {
                $unwind: "$products",
            },
            {
                $match: {
                    $expr: {
                        $lt: ["$products.quantity", "$products.Threshold"],
                    },
                },
            },
            { $project: { products: 1, _id: 1 } },
        ]);
        return products;
    }
    catch (err) {
        throw { message: "something went wrong", e: err };
    }
};
const updateShopAverageRating = async (shopId) => {
    const result = await shop_schema_1.ShopModel.aggregate([
        { $match: { _id: new mongoose_1.default.mongo.ObjectId(shopId) } },
        { $unwind: "$ratings" },
        { $group: { _id: shopId, average: { $avg: "$ratings" } } },
    ]);
    const averageRating = result[0].average;
    averageRating.toFixed(2);
    await shop_schema_1.ShopModel.updateOne({ _id: shopId }, { $set: { Rating: averageRating } });
    return result;
};
exports.default = {
    create,
    findOne,
    findById,
    findByIdAndUpdate,
    find,
    RevenueWise,
    PointWise,
    findAll,
    update,
    addShopRating,
    findLowProducts,
    updateShopAverageRating
};
