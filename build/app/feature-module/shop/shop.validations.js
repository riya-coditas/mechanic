"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PARAM_VALIDATION = exports.FINDALL_SHOP_VALIDATION = exports.RatingValidator = exports.ShopValidator = void 0;
const express_validator_1 = require("express-validator");
const validate_1 = require("../../utils/validate");
exports.ShopValidator = [
    (0, express_validator_1.body)("owner")
        .isString()
        .withMessage("String value required"),
    (0, express_validator_1.body)("address")
        .isString()
        .withMessage("String value required"),
    validate_1.validate,
];
exports.RatingValidator = [
    (0, express_validator_1.body)("ratings")
        .isInt({ min: 1, max: 5 })
        .optional(),
    validate_1.validate,
];
exports.FINDALL_SHOP_VALIDATION = [
    (0, express_validator_1.query)("sort")
        .optional()
        .isString()
        .withMessage("sort field to sort"),
    (0, express_validator_1.query)("limit")
        .optional()
        .isNumeric()
        .withMessage("limit field to limit"),
    (0, express_validator_1.query)("page")
        .optional()
        .isNumeric()
        .withMessage("page field to paginate"),
    validate_1.validate,
];
exports.PARAM_VALIDATION = [
    (0, express_validator_1.param)("id")
        .isString()
        .withMessage("id parameter is necessary"),
    validate_1.validate,
];
