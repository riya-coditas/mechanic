"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const response_handler_1 = require("../../utils/response_handler");
const shop_services_1 = __importDefault(require("./shop.services"));
const mongoose_1 = __importDefault(require("mongoose"));
const role_types_1 = require("../role/role.types");
const permit_1 = require("../../utils/permit");
const shop_validations_1 = require("./shop.validations");
const router = (0, express_1.Router)();
// User can view all shops
router.get("/", shop_validations_1.FINDALL_SHOP_VALIDATION, async (req, res, next) => {
    try {
        const filter = req.query;
        const result = await shop_services_1.default.find(filter);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
// Admin can view all shops
router.get("/AllShops", shop_validations_1.FINDALL_SHOP_VALIDATION, (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const filter = req.query;
        const result = await shop_services_1.default.findAll(filter);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.post("/CreateShop", shop_validations_1.ShopValidator, (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const result = await shop_services_1.default.create(req.body);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.patch("/Update/:id", shop_validations_1.PARAM_VALIDATION, (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const id = req.params.id;
        const update = req.body;
        const result = await shop_services_1.default.findByIdAndUpdate(new mongoose_1.default.mongo.ObjectId(id), update);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.delete("/Delete/:id", (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const id = req.params.id;
        const result = await shop_services_1.default.deleteShop(id);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.post("/LowProduct", (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const result = await shop_services_1.default.LowProducts();
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.post("/Redem/:id", (0, permit_1.permit)([role_types_1.Roles.owner.toString()]), async (req, res, next) => {
    try {
        const shopid = req.params.id;
        const rewardId = req.body.rewardId;
        const result = await shop_services_1.default.redemReward(shopid, rewardId);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.get("/Revenue-wise", (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const result = await shop_services_1.default.RevenueWise();
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.get("/Point-wise", (0, permit_1.permit)([role_types_1.Roles.admin.toString()]), async (req, res, next) => {
    try {
        const result = await shop_services_1.default.PointWise();
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.post("/rating/:id", shop_validations_1.RatingValidator, async (req, res, next) => {
    try {
        const shopId = req.params.id;
        const ratings = req.body.ratings;
        const AddRating = await shop_services_1.default.addShopRating(shopId, ratings);
        const result = await shop_services_1.default.updateShopAverageRating(shopId);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
exports.default = router;
