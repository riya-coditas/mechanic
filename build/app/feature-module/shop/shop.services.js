"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const shop_repo_1 = __importDefault(require("./shop.repo"));
const shop_responses_1 = require("./shop.responses");
const mongoose_1 = __importDefault(require("mongoose"));
const reward_services_1 = __importDefault(require("../rewards/reward.services"));
const reward_reponses_1 = require("../rewards/reward.reponses");
const create = (shop) => shop_repo_1.default.create(shop);
const findOne = async (filters) => {
    const shop = await shop_repo_1.default.findOne(filters);
    if (!shop)
        throw shop_responses_1.SHOP_RESPONSES.NOT_FOUND;
    return shop;
};
const findById = async (id) => {
    const shop = await shop_repo_1.default.findById(id);
    return shop;
};
const find = async (query) => {
    const shop = shop_repo_1.default.find(query);
    return shop;
};
const findAll = async (query) => {
    const shop = shop_repo_1.default.findAll(query);
    return shop;
};
const findByIdAndUpdate = (filter, update, options = {}) => {
    return shop_repo_1.default.findByIdAndUpdate(filter, update);
};
const deleteShop = async (id) => {
    const record = await shop_repo_1.default.update({ _id: new mongoose_1.default.mongo.ObjectId(id) }, { isDeleted: true });
    if (!record)
        throw shop_responses_1.SHOP_RESPONSES.NOT_FOUND;
    return record;
};
const redemReward = async (id, rewardId) => {
    const shop = await shop_repo_1.default.findById(id);
    if (!shop) {
        throw shop_responses_1.SHOP_RESPONSES.NOT_FOUND;
    }
    const reward = await reward_services_1.default.findById(rewardId);
    if (!reward) {
        throw reward_reponses_1.REWARD_RESPONSES.NOT_FOUND;
    }
    if (shop.points < reward.points) {
        throw shop_responses_1.SHOP_RESPONSES.INVALID;
    }
    shop.points -= reward.points;
    shop.reward.push({
        _id: reward._id,
        name: reward.name,
        points: reward.points
    });
    await shop.save();
    return reward;
};
const RevenueWise = async () => await shop_repo_1.default.RevenueWise();
const PointWise = async () => await shop_repo_1.default.PointWise();
const addShopRating = async (shopId, ratings) => {
    await shop_repo_1.default.addShopRating(shopId, ratings);
};
const LowProducts = async () => {
    const products = await shop_repo_1.default.findLowProducts();
    return products;
};
const updateShopAverageRating = async (shop_id) => await shop_repo_1.default.updateShopAverageRating(shop_id);
exports.default = {
    create,
    findOne,
    findById,
    findByIdAndUpdate,
    find,
    findAll,
    redemReward,
    RevenueWise,
    PointWise,
    deleteShop,
    addShopRating,
    updateShopAverageRating,
    LowProducts
};
