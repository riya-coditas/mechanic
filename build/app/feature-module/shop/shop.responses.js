"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SHOP_RESPONSES = void 0;
exports.SHOP_RESPONSES = {
    NOT_FOUND: {
        statusCode: 404,
        message: "Shop not found",
    },
    INVALID: {
        statusCode: 400,
        message: "Invalid",
    },
};
