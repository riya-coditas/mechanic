"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShopModel = void 0;
const mongoose_1 = require("mongoose");
const base_schema_1 = require("../../utils/base-schema");
const inventory_schema_1 = require("../Inventory/inventory.schema");
const reward_schema_1 = require("../rewards/reward.schema");
const ShopSchema = new base_schema_1.BaseSchema({
    owner: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Users',
        required: true
    },
    address: {
        type: String,
        required: true
    },
    products: [inventory_schema_1.ProductSchema],
    reward: [reward_schema_1.RewardSchema],
    points: {
        type: Number,
        default: 0
    },
    ratings: [{
            type: Number,
        }],
    Rating: {
        type: Number,
        default: 0
    },
    totalRevenue: {
        type: Number,
        default: 0
    }
});
exports.ShopModel = (0, mongoose_1.model)("Shops", ShopSchema);
