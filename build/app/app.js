"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.startServer = void 0;
const express_1 = __importDefault(require("express"));
const routes_1 = require("./routes/routes");
const connections_mongo_1 = require("./connections/connections.mongo");
const startServer = async () => {
    try {
        const app = (0, express_1.default)();
        await (0, connections_mongo_1.connectToMongo)();
        (0, routes_1.registerRoutes)(app);
        const { PORT } = process.env;
        app.listen(PORT || 1452, () => console.log(`PORT STARTED ON PORT ${PORT || 1452}`));
    }
    catch (e) {
        console.log("COULD NOT START THE SERVER");
        console.log(e);
        process.exit(1);
    }
};
exports.startServer = startServer;
