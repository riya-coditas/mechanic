import { config } from "dotenv"
config()

import { startServer } from "./app/app"
startServer();


import { Roles } from "./app/feature-module/role/role.types";
import authService from "./app/feature-module/auth/auth.services";


// const populateDb = async () => {
//   const admin = {
//     name: "admin",
//     email: "admin@gmail.com",
//     password: "admin1",
//     role: Roles.admin,
//   };

//   await authService.register(admin);
// };

// populateDb();
