import { Schema, model } from "mongoose";
import { BaseSchema } from "../../utils/base-schema";
import { IShop } from "./shop.types"
import { ProductSchema } from "../Inventory/inventory.schema";
import { RewardSchema } from "../rewards/reward.schema";

const ShopSchema = new BaseSchema({
    owner: {
        type:Schema.Types.ObjectId,
        ref:'Users',
        required:true
    },
    address:{
        type:String,
        required:true
    },

    products:[ProductSchema],

    reward : [RewardSchema],

    points:{
        type:Number,
        default:0
    },

    ratings:[{
        type:Number,
    }],

    Rating:{
        type:Number,
        default:0
    },

    totalRevenue:{
        type:Number,
        default:0
    }
}
)
type ShopDocument = Document & IShop;
export const ShopModel = model<ShopDocument>("Shops",ShopSchema);

