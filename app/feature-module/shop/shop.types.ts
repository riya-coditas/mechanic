import { ObjectId } from "mongoose";
import { IProduct } from "../Inventory/inventory.types";
import { IReward } from "../rewards/reward.types";

export interface IShop {
  _id: string;
  owner: string;
  address: string;
  products: IProduct[];
  reward: IReward[];
  points?: number;
  ratings:number[];
  Rating: number;
  totalRevenue:number
}
