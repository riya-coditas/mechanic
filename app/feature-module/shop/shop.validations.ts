import { body, param, query } from "express-validator";
import { validate } from "../../utils/validate";

export const ShopValidator = [
  body("owner")
  .isString()
  .withMessage("String value required"),

  body("address")
  .isString()
  .withMessage("String value required"),

  body("points")
  .optional()
  .isInt()
  .withMessage("Numeric value required"),

  body("totalRevenue")
  .optional()
  .isInt()
  .withMessage("Numeric value required"),

  validate,
];

export const RatingValidator = [
  body("ratings")
  .isInt({ min:1 ,max: 5 })
  .optional(),
  
  validate,
];

export const FINDALL_SHOP_VALIDATION = [
  query("sort")
  .optional()
  .isString()
  .withMessage("sort field to sort"),

  query("limit")
  .optional()
  .isNumeric()
  .withMessage("limit field to limit"),

  query("page")
  .optional()
  .isNumeric()
  .withMessage("page field to paginate"),

  validate,
];

export const PARAM_VALIDATION = [
  param("id")
  .isString()
  .withMessage("id parameter is necessary"),
  
  validate,
];
