import { ObjectId } from "bson";
import { ShopModel } from "./shop.schema";
import { IShop } from "./shop.types";
import mongoose, { FilterQuery, QueryOptions, Types, UpdateQuery } from "mongoose";
import { SHOP_RESPONSES } from "./shop.responses";
import shopServices from "./shop.services";
import { generatePipeline } from "../../utils/pipeline";


const create = (shop : IShop) => ShopModel.create(shop);


const findOne = async (filters:Partial<IShop>) => {
    try {
        return await ShopModel.findOne({
            ...filters,
            isDeleted:false
        })
    } catch (err) {
        throw { message: 'something went wrong', e: err } 
    } 
}

const findById = async (id:string) => {
    const shop = await ShopModel.findById(id);
    if(!shop){
        throw SHOP_RESPONSES.NOT_FOUND
    }
    return shop;
}

const find = async (query:any) => {
  const pipeline = generatePipeline(query);
  return await ShopModel.aggregate([
      { $match: { isDeleted: false }},
      { $project: { owner:1 , address:1 , rating:1   }},
      ...pipeline,
    ]);  
};

const findByIdAndUpdate = (filter: FilterQuery<IShop>,update: UpdateQuery<IShop>,options: QueryOptions = {}
) => {
  return ShopModel.findByIdAndUpdate(filter, update, {
    ...options,
    new: true,
  });
};

const update = async (filter: FilterQuery<IShop>, update: UpdateQuery<IShop>) =>
  await ShopModel.updateOne(filter, update);


const findAll = async (query: any) => {
  try {
    const pipeline = generatePipeline(query);
    return await ShopModel.aggregate([
      {
        $match: { isDeleted: false },
      },
      ...pipeline,
    ]);
  } catch (err) {
    throw { message: "something went wrong", e: err };
  }
};

const RevenueWise = async() => await ShopModel.find().sort({totalRevenue:-1})

const PointWise = async () => await ShopModel.find().sort({ points: -1 });


const addShopRating = async (shopId: string, ratings: number) => {
  await ShopModel.findByIdAndUpdate(shopId, { $push: { ratings: ratings } });
};

const findLowProducts = async () => {
  try {
    const products = await ShopModel.aggregate([
      {
        $unwind: "$products",
      },
      {
        $match: {
          $expr: {
            $lt: ["$products.quantity", "$products.Threshold"],
          },
        },
      },
      { $project: { products: 1, _id: 1 } },
    ]);
    return products;
  } catch (err) {
    throw { message: "something went wrong", e: err };
  }
};



const updateShopAverageRating = async (shopId: string) => {
   const result = await ShopModel.aggregate([
      { $match: { _id: new mongoose.mongo.ObjectId(shopId) } },
      { $unwind: "$ratings" },
      { $group: { _id: shopId, average: { $avg: "$ratings" } } },
    ]);

      const averageRating = result[0].average;
      averageRating.toFixed(2)
      await ShopModel.updateOne(
        { _id: shopId },
        { $set: { Rating: averageRating } }
      );
    return result;
};


export default{
    create,
    findOne,
    findById,
    findByIdAndUpdate,
    find,
    RevenueWise,
    PointWise,
    findAll,
    update,
    addShopRating,
    findLowProducts,
    updateShopAverageRating
}