import { NextFunction, Request, Response, Router, request } from "express";
import { ResponseHandler } from "../../utils/response_handler";
import shopServices from "./shop.services";
import mongoose, { Types } from "mongoose";
import { Roles } from "../role/role.types";
import { permit } from "../../utils/permit";
import { FINDALL_SHOP_VALIDATION, PARAM_VALIDATION, RatingValidator, ShopValidator } from "./shop.validations";

const router = Router()

// User can view all shops
router.get( "/" , FINDALL_SHOP_VALIDATION ,async (req:Request, res:Response, next:NextFunction) => {
    try {
      const filter = req.query;
      const result = await shopServices.find(filter);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

// Admin can view all shops
router.get("/AllShops", FINDALL_SHOP_VALIDATION, permit([Roles.admin.toString()]),async (req:Request, res:Response, next:NextFunction) => {
  try {
    const filter = req.query;
    const result = await shopServices.findAll(filter);
    res.send(new ResponseHandler(result));
  } catch (e) {
    next(e);
  }
});

router.post("/CreateShop", ShopValidator, permit([Roles.admin.toString()]), async (req:Request,res:Response,next:NextFunction) => {
    try{
        const result = await shopServices.create(req.body);
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    } 
})

router.patch("/Update/:id",PARAM_VALIDATION,permit([Roles.admin.toString()]),async (req:Request, res:Response, next:NextFunction) => {
    try {
      const id = req.params.id;
      const update = req.body;
      const result = await shopServices.findByIdAndUpdate(new mongoose.mongo.ObjectId(id),update);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.delete("/Delete/:id",permit([Roles.admin.toString()]),async (req, res, next) => {
    try {
      const id = req.params.id;
      const result = await shopServices.deleteShop(id);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);


router.post("/LowProduct",permit([Roles.admin.toString()]), async (req: Request, res: Response, next: NextFunction) => {
    try {
      const result = await shopServices.LowProducts();
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.post("/Redem/:id", permit([Roles.owner.toString()]), async (req,res,next) => {
    try {
        const shopid = req.params.id;
        const rewardId = req.body.rewardId;
        const result = await shopServices.redemReward(shopid,rewardId)
        res.send(new ResponseHandler(result));
    } catch (e) {
      next(e)
    } 
})

router.get("/Revenue-wise",permit([Roles.admin.toString()]), async (req, res, next) => {
  try {
    const result = await shopServices.RevenueWise();
    res.send(new ResponseHandler(result));
  } catch (e) {
    next(e);
  }
});

router.get("/Point-wise",permit([Roles.admin.toString()]), async (req, res, next) => {
  try {
    const result = await shopServices.PointWise();
    res.send(new ResponseHandler(result));
  } catch (e) {
    next(e);
  }
});


router.post("/rating/:id", RatingValidator ,async (req:Request, res:Response, next:NextFunction) => {
    try {
      const shopId = req.params.id;
      const ratings = req.body.ratings;
      const AddRating = await shopServices.addShopRating(shopId,ratings)
      const result = await shopServices.updateShopAverageRating(shopId);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

export default router;