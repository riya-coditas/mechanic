import shopRepo from "./shop.repo";
import { IShop } from "./shop.types";
import { SHOP_RESPONSES } from "./shop.responses";
import productServices from "../Inventory/inventory.services";
import mongoose, { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import { ShopModel } from "./shop.schema";
import rewardServices from "../rewards/reward.services";
import { REWARD_RESPONSES } from "../rewards/reward.reponses";
import { IReward } from "../rewards/reward.types";


const create = (shop : IShop) => shopRepo.create(shop);

const findOne = async (filters:Partial<IShop>) => {
    const shop = await shopRepo.findOne(filters);
    if(!shop) throw SHOP_RESPONSES.NOT_FOUND
    
    return shop;
}

const findById = async (id:string) =>{
    const shop = await shopRepo.findById(id)
    return shop;
}

const find = async (query:any) => {
    const shop = shopRepo.find(query)
    return shop
}

const findAll = async (query:any) => {
  const shop = shopRepo.findAll(query);
  return shop;
};

const findByIdAndUpdate = ( filter: FilterQuery<IShop>,update: UpdateQuery<IShop>,options: QueryOptions = {}
) => {
  return shopRepo.findByIdAndUpdate(filter, update);
};

const deleteShop = async (id: string) => {
  const record = await shopRepo.update(
    { _id: new mongoose.mongo.ObjectId(id) },
    { isDeleted: true }
  );
  if (!record) throw SHOP_RESPONSES.NOT_FOUND;
  return record;
};


const redemReward = async (id:string,rewardId:string) => {
    const shop = await shopRepo.findById(id)
    if (!shop) {
      throw SHOP_RESPONSES.NOT_FOUND;
    }

    const reward = await rewardServices.findById(rewardId)
    if(!reward){
       throw REWARD_RESPONSES.NOT_FOUND
    }

    if (shop.points! < reward.points) {
      throw SHOP_RESPONSES.INVALID;
    }
    shop.points! -= reward.points

    shop.reward.push({
       _id: reward._id,
       name : reward.name,
       points : reward.points
    })

    await shop.save()

    return reward
  
}

const RevenueWise = async () => await shopRepo.RevenueWise();

const PointWise = async () => await shopRepo.PointWise();

const addShopRating = async (shopId: string, ratings: number) => {
  await shopRepo.addShopRating(shopId,ratings);
};

const LowProducts = async () => {
  const products = await shopRepo.findLowProducts();
  return products;
};

const updateShopAverageRating = async(shop_id:string) => await shopRepo.updateShopAverageRating(shop_id);

export default{
    create,
    findOne,
    findById,
    findByIdAndUpdate,
    find,
    findAll,
    redemReward,
    RevenueWise,
    PointWise,
    deleteShop,
    addShopRating,
    updateShopAverageRating,
    LowProducts
}