export const SHOP_RESPONSES = {
  NOT_FOUND: {
    statusCode: 404,
    message: "Shop not found",
  },
  INVALID: {
    statusCode: 400,
    message: "Invalid",
  },
};