import { NextFunction, Request, Response, Router } from "express";
import roleServices from "./role.services";
import { ResponseHandler } from "../../utils/response_handler";
import { permit } from "../../utils/permit";
import { Roles } from "./role.types";
import mongoose from "mongoose";
import { FINDALL_ROLE_VALIDATION, PARAM_VALIDATION, RoleValidator } from "./role.validations";

const router = Router()


router.post("/registerRole",RoleValidator, async (req:Request,res:Response,next:NextFunction) => {
    try{
        const result = await roleServices.create(req.body);
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
})

router.get("/",FINDALL_ROLE_VALIDATION,permit([Roles.admin.toString()]),async (req: Request, res: Response, next: NextFunction) => {
    try {
      const filter = req.query;
      const result = await roleServices.find(filter);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.patch("/Update/:id",PARAM_VALIDATION,permit([Roles.admin.toString()]),async (req:Request, res:Response, next:NextFunction) => {
    try {
      const id = req.params.id;
      const update = req.body;
      const result = await roleServices.findByIdAndUpdate(new mongoose.mongo.ObjectId(id),update);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.delete("/Delete/:id",PARAM_VALIDATION,permit([Roles.admin.toString()]),async (req: Request, res: Response, next:NextFunction) => {
    try {
      const id = req.params.id;
      const result = await roleServices.deleteRole(id);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);


export default router;