import mongoose, { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import roleRepo from "./role.repo";
import { IRole } from "./role.types";
import { ROLE_RESPONSE } from "./role.response";

const create = (role : IRole) => roleRepo.create(role);

const find = async (query: any) => {
  const products = await roleRepo.find(query);
  return products;
};

const findByIdAndUpdate = (
  filter: FilterQuery<IRole>,
  update: UpdateQuery<IRole>,
  options: QueryOptions = {}
) => {
  return roleRepo.findByIdAndUpdate(filter, update, { ...options, new: true });
};

const deleteRole = async (id: string) => {
  const record = await roleRepo.update(
    { _id: new mongoose.mongo.ObjectId(id) },
    { isDeleted: true }
  );
  if (!record) throw ROLE_RESPONSE.NOT_FOUND;
  return record;
};

export default{
    create,
    find,
    findByIdAndUpdate,
    deleteRole
}