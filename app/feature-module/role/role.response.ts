export const ROLE_RESPONSE = {
  NOT_FOUND: {
    statusCode: 404,
    message: "Role not found",
  },
};
