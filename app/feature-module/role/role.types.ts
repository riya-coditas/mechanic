import mongoose from "mongoose"

export interface IRole {
    _id: string,
    name: string
}

export const Roles = {
  admin: new mongoose.mongo.ObjectId("6445bbca29e6df1c9961d359"),
  owner: new mongoose.mongo.ObjectId("6445bbd029e6df1c9961d35b"),
};
