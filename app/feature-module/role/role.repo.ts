import { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import { RoleModel } from "./role.schema";
import { IRole } from "./role.types";
import { generatePipeline } from "../../utils/pipeline";


const create = (role : IRole) => RoleModel.create(role);


const find = async (query: any) => {
  try {
    const pipeline = generatePipeline(query);
    return await RoleModel.aggregate([
      {
        $match: { isDeleted: false },
      },
      ...pipeline,
    ]);
  } catch (err) {
    throw { message: "something went wrong", e: err };
  }
};

const findByIdAndUpdate = (filter: FilterQuery<IRole>,update: UpdateQuery<IRole>,options: QueryOptions = {}
) => {
  return RoleModel.findByIdAndUpdate(filter, update, { ...options, new: true });
};


const update = async (filter: FilterQuery<IRole>, update: UpdateQuery<IRole>) =>
  await RoleModel.updateOne(filter, update);


export default{
    create,
    find,
    findByIdAndUpdate,
    update
}