import { Schema, model } from "mongoose";
import { BaseSchema } from "../../utils/base-schema";
import { IRequirements } from "./requirements.type";


const RequirementSchema = new BaseSchema({
    shop_id: {
        type : Schema.Types.ObjectId,
        ref:'Shops',
        required:true
    },
    product_id:{
        type:Schema.Types.ObjectId,
        ref:"Product",
        required:true
    },
    quantity:{
        type:Number,
        required:true
    },
    isApproved:{
        type:Boolean,
        default:false
    }
})


type RequirementDocument = Document & IRequirements;
export const RequirementModel = model<RequirementDocument>("Requirements",RequirementSchema)