import mongoose, { FilterQuery, ObjectId, QueryOptions, UpdateQuery } from "mongoose";
import shopServices from "../shop/shop.services";
import requirementsRepo from "./requirements.repo";
import { IRequirements } from "./requirements.type";
import {REQUIREMENT_RESPONSES } from "./requirements.responses"
import { ProductModel } from "../Inventory/inventory.schema";


const create = async (order:IRequirements,id:string) => {
     const shop = await shopServices.findOne({_id : id})
     order.shop_id = shop.id;
     const orderplaced = await requirementsRepo.create(order)
     return orderplaced
}


const findByIdAndUpdate = (filter: FilterQuery<IRequirements>,update: UpdateQuery<IRequirements>,options: QueryOptions = {}
) => {
  return requirementsRepo.findByIdAndUpdate(filter, update);
};

const find = async (query: any) => {
  const products = await requirementsRepo.find(query);
  return products;
};

const deleteRequirement = async (id: string) => {
  const record = await requirementsRepo.update(
    { _id: new mongoose.mongo.ObjectId(id) },
    { isDeleted: true }
  );
  if (!record) throw REQUIREMENT_RESPONSES.NOT_FOUND;
  return record;
};


const approveRequirement = async (requirementId: string) => {
  const requirement = await requirementsRepo.findById(requirementId);
  if(!requirement){
    throw REQUIREMENT_RESPONSES.NOT_FOUND;
  }

  const product = await ProductModel.findById(requirement.product_id);
  const shop = await shopServices.findById(requirement.shop_id);
  
  if (!product || !shop || product.quantity < requirement.quantity) {
    throw REQUIREMENT_RESPONSES.INVALID
    
  }

  let existingProduct = shop.products.find((p)=>p._id.toString() == product._id.toString());
  if(existingProduct){
    existingProduct.quantity += requirement.quantity
  } else {
  shop.products.push({
    _id: product._id,
    name:product.name,
    price:product.price,
    quantity: requirement.quantity,
    Threshold: product.Threshold,
    Productpoints:product.Productpoints
  });

}  
  shop.save();

  product.quantity -= requirement.quantity;
  await product.save();

    const approve = await requirementsRepo.findByIdAndUpdate(
      { _id: new mongoose.mongo.ObjectId(requirementId) },
      { isApproved: true },
      { new: true }
    );

  await requirement.save();
  return approve;
};


export default{
    create,
    approveRequirement,
    findByIdAndUpdate,
    find,
    deleteRequirement
}