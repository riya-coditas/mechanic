import { body, param, query } from "express-validator";
import { validate } from "../../utils/validate";

export const RequirementValidator = [
  body("product_id").isString().withMessage("String value required"),

  body("quantity").isInt().withMessage("Numeric value required"),

  body("isApproved")
    .optional()
    .isBoolean()
    .withMessage("Boolean value required"),

  validate,
];

export const FINDALL_REQUIREMENT_VALIDATION = [
  query("sort")
  .optional()
  .isString()
  .withMessage("sort field to sort"),

  query("limit")
  .optional()
  .isNumeric()
  .withMessage("limit field to limit"),

  query("page")
  .optional()
  .isNumeric()
  .withMessage("page field to paginate"),

  validate,
];

export const PARAM_VALIDATION = [
  param("id")
  .isString()
  .withMessage("id parameter is necessary"),
  
  validate,
];
