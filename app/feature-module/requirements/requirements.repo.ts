import { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import { RequirementModel } from "./requirements.schema";
import { IRequirements } from "./requirements.type";
import { generatePipeline } from "../../utils/pipeline";


const create = (requirement : IRequirements) => RequirementModel.create(requirement);

const findByIdAndUpdate = (filter: FilterQuery<IRequirements>,update: UpdateQuery<IRequirements>,options: QueryOptions = {}
) => {
  return RequirementModel.findByIdAndUpdate(filter, update, {
    ...options,
    new: true,
  });
};

const findById = async (id: string) => {
    const requirement = await RequirementModel.findById(id).exec();
    return requirement;
}

const find = async (query: any) => {
  try {
    const pipeline = generatePipeline(query);
    return await RequirementModel.aggregate([
      {
        $match: { isDeleted: false },
      },
      ...pipeline,
    ]);
  } catch (err) {
    throw { message: "something went wrong", e: err };
  }
};

const update = async (
  filter: FilterQuery<IRequirements>,
  update: UpdateQuery<IRequirements>
) => await RequirementModel.updateOne(filter, update);

export default{
    create,
    findByIdAndUpdate,
    findById,
    find,
    update
}