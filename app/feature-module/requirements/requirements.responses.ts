export const REQUIREMENT_RESPONSES = {
    NOT_FOUND : {
        statusCode : 404,
        message :"Requirement Not Found"
    },

    INVALID : {
        statusCode : 404,
        message :"Product not available in inventory or quantity not enough"
    },
}