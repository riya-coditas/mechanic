import { NextFunction, Request, Response, Router } from "express";
import requirementsServices from "./requirements.services";
import { ResponseHandler } from "../../utils/response_handler";
import { Roles } from "../role/role.types";
import mongoose from "mongoose";
import { FINDALL_REQUIREMENT_VALIDATION, PARAM_VALIDATION, RequirementValidator } from "./requirement.validations";
import { permit } from "../../utils/permit";

const router = Router()

router.get("/", FINDALL_REQUIREMENT_VALIDATION,permit([Roles.admin.toString()]), async (req:Request, res:Response, next:NextFunction) => {
  try {
    const filter = req.query;
    const result = await requirementsServices.find(filter);
    res.send(new ResponseHandler(result));
  } catch (e) {
    next(e);
  }
});

router.post("/AddRequirements/:id", PARAM_VALIDATION ,RequirementValidator ,permit([Roles.owner.toString()]), async (req:Request,res:Response,next:NextFunction) => {
    try{
        const id = req.params.id
        const requirements = req.body
         const result = await requirementsServices.create(requirements,id);
         res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    } 
})

router.post("/approve/:id", PARAM_VALIDATION, permit([Roles.admin.toString()]), async (req:Request,res:Response,next:NextFunction) => {
  try {
    const requirementId = req.params.id;
    const result = await requirementsServices.approveRequirement(requirementId);
    res.send(new ResponseHandler(result))
  } catch(e){
        next(e)
    } 
});

router.patch( "/Update/:id",PARAM_VALIDATION, permit([Roles.admin.toString()]),async (req:Request, res:Response, next:NextFunction) => {
    try {
      const id = req.params.id;
      const update = req.body;
      const result = await requirementsServices.findByIdAndUpdate(new mongoose.mongo.ObjectId(id),update);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.delete("/Delete/:id",PARAM_VALIDATION,permit([Roles.owner.toString()]),async (req:Request, res:Response, next:NextFunction) => {
    try {
      const id = req.params.id;
      const result = await requirementsServices.deleteRequirement(id);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

export default router;