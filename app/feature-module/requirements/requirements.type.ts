import { ObjectId } from "mongoose";


export interface IRequirements {
    _id: string,
    shop_id:string,
    product_id:string,
    quantity:number,
    isApproved?:boolean
}