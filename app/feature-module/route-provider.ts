import authRouter from "../feature-module/auth/auth.routes";
import userRouter from "../feature-module/user/user.routes"
import roleRouter from "../feature-module/role/role.routes";
import productRouter from "../feature-module/Inventory/inventory.routes";
import shopRouter from "../feature-module/shop/shop.routes";
import requirementRouter from "../feature-module/requirements/requirements.routes";
import saleRouter from "../feature-module/sales/sale.routes";
import rewardRouter from "../feature-module/rewards/reward.routes";

export default { 
     roleRouter, 
     authRouter,
     userRouter,
     productRouter,
     shopRouter,
     requirementRouter,
     saleRouter,
     rewardRouter
};
