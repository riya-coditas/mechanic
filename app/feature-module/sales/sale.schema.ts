import { Schema, Types, model } from "mongoose";
import { BaseSchema } from "../../utils/base-schema";
import { ISale } from "./sale.type";

const SaleSchema = new BaseSchema({
    shopid : {
        type : Schema.Types.ObjectId,
        required : true
    },
    product_id :{
         type : Schema.Types.ObjectId,
         required : true
    },
    product_name : {
        type:String
    },
    quantity : {
        type : Number,
        required : true
    },
    Amount : {
        type : Number
    },
    points : {
        type : Number,
        default : 0
    },
    isValidated : {
        type : Boolean,
        default : false
    },
    isRejected : {
        type : Boolean,
        default : false
    }
})

type SaleDocument = Document & ISale;

export const SaleModel = model<SaleDocument>("Sales",SaleSchema);
