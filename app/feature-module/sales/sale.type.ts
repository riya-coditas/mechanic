import { ObjectId } from "mongoose";


export interface ISale{
    _id:string,
    shopid:string
    product_id:string,
    product_name:string,
    quantity:number,
    Amount:number,
    points:number,
    isValidated:boolean
    isRejected:boolean
}