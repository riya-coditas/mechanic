export const SALE_RESPONSES = {
  NOT_FOUND: {
    statusCode: 404,
    message: "Not found",
  },
  INVALID: {
    statusCode: 400,
    message: "Invalid",
  },
  PRODUCT_NOT_FOUND: {
    statusCode: 404,
    message: "Product Not found",
  },
};