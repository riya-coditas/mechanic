import { NextFunction, Request, Response, Router } from "express";
import { ResponseHandler } from "../../utils/response_handler";
import saleServices from "./sale.services";
import saleRepo from "./sale.repo";
import { Roles } from "../role/role.types";
import mongoose from "mongoose";
import { permit } from "../../utils/permit";
import { FINDALL_SALE_VALIDATION, PARAM_VALIDATION, SalesValidator } from "./sale.validator";


const router = Router()

router.post("/Sales", SalesValidator ,permit([Roles.owner.toString()]), async (req:Request,res:Response,next:NextFunction) => {
    try{
        const result = await saleServices.sellProduct(req.body);
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    } 
})

router.get("/",FINDALL_SALE_VALIDATION,permit([Roles.admin.toString()]),async (req:Request, res:Response, next:NextFunction) => {
    try {
      const filter = req.query;
      const result = await saleServices.find(filter);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);


router.patch("/Update/:id",PARAM_VALIDATION,permit([Roles.admin.toString()]),async (req:Request, res:Response, next:NextFunction) => {
    try {
      const id = req.params.id;
      const update = req.body;
      const result = await saleServices.findByIdAndUpdate(new mongoose.mongo.ObjectId(id),
        update
      );
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);


router.delete("/Delete/:id",PARAM_VALIDATION,permit([Roles.admin.toString()]),async (req:Request, res:Response, next:NextFunction) => {
    try {
      const id = req.params.id;
      const result = await saleServices.deleteSale(id);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);


router.get("/ViewPendingSales",permit([Roles.admin.toString()]), async (req,res,next) => {
    try{
        const result = await saleServices.ViewPendingSales();
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    } 
})

router.put("/Approve/:id",PARAM_VALIDATION,permit([Roles.admin.toString()]), async (req:Request, res:Response, next:NextFunction) => {
  try {
    const id = req.params.id
    const result = await saleServices.approveSale(id);
    res.send(new ResponseHandler(result));
  } catch (e) {
    next(e);
  }
});

router.put("/Reject/:id",PARAM_VALIDATION,permit([Roles.admin.toString()]),async (req: Request, res: Response, next: NextFunction) => {
    try {
      const id = req.params.id;
      const result = await saleServices.rejectSale(id);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.get("/Item-wise",permit([Roles.admin.toString()]), async (req, res, next) => {
  try {
    const result = await saleRepo.itemwise();
    res.send(new ResponseHandler(result));
  } catch (e) {
    next(e);
  }
});

export default router;