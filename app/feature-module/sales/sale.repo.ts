import mongoose, { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import { SALE_RESPONSES } from "./sale.responses";
import { SaleModel } from "./sale.schema";
import { ISale } from "./sale.type";
import { generatePipeline } from "../../utils/pipeline";


const create = (sale : ISale) => SaleModel.create(sale);

const findById = async (id:string) => {
    const sale = await SaleModel.findById(id);
    if(!sale){
        throw SALE_RESPONSES.NOT_FOUND
    }
    return sale;
}

const findByIdAndUpdate = (filter: FilterQuery<ISale>,update: UpdateQuery<ISale>,options: QueryOptions = {}
) => {
  return SaleModel.findByIdAndUpdate(filter, update, {
    ...options,
    new: true,
  });
};

const find = async (query: any) => {
  try {
    const pipeline = generatePipeline(query);
    return await SaleModel.aggregate([
      {
        $match: { isDeleted: false },
      },
      ...pipeline,
    ]);
  } catch (err) {
    throw { message: "something went wrong", e: err };
  }
};


const update = async (
  filter: FilterQuery<ISale>,
  update: UpdateQuery<ISale>
) => await SaleModel.updateOne(filter, update);


const ViewPendingSales = async () => {
    const saleRecords = await SaleModel.find({isValidated: false,isRejected: false});
    const pendingRevenue = await SaleModel.aggregate([
    {
        $match: {isValidated: false,isRejected:false}
    },
    {
        $group:{
            _id:"",
            totalRevenue: {$sum: "$Amount"},
            totalPoints : {$sum : "$points"}
        }
    } 
])
return {pendingRevenue,saleRecords}
}

const approveSale = async() => {
  const filter = { isValidated: false, isRejected: false };
  const update = { isValidated : true };
  const approve = await SaleModel.updateMany(filter,update)

  const CalculateRevenue = await SaleModel.aggregate([
    {
      $match: { isValidated: true },
    },
    {
      $group: {
        _id: "",
        totalRevenue: { $sum: "$Amount" },
      },
    },
  ]);
  const totalRevenue = CalculateRevenue[0].totalRevenue;
  return { approve, totalRevenue };
}

const rejectSale = async () => {
  const filter = {  isValidated: false, isRejected: false }; 
  const update = { isRejected: true };
  const rejected = await SaleModel.updateMany(filter, update);
  return rejected;
};

const itemwise = async() => {
   const result = await SaleModel.aggregate([
    {$match:{isValidated:true}},
    {$group:{
        _id:"$product_id",
        product_name :{$first:"$product_name"},
        totalQuantity : {$sum: "$quantity"},
        shop_id : {$first : "$shopid"}
    }
  },{
    $sort:{ totalQuantity: -1 }
   }
 ])
 return result
} 

export default{
    create,
    findById,
    findByIdAndUpdate,
    ViewPendingSales,
    rejectSale,
    approveSale,
    itemwise,
    update,
    find
}