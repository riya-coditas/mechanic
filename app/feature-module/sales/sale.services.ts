import { ISale } from "./sale.type";
import saleRepo from "./sale.repo";
import shopServices from "../shop/shop.services";
import { SALE_RESPONSES } from "./sale.responses";
import mongoose, { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";


const sellProduct = async (sale: ISale) => {
  const shop = await shopServices.findById(sale.shopid);
  const product = shop.products.find((p) => p._id == sale.product_id)

  if (!product || product.quantity < sale.quantity) {
    throw SALE_RESPONSES.INVALID
  }

  sale.product_name = product.name
  sale.Amount = product!.price * sale.quantity 
  sale.points = product!.Productpoints * sale.quantity

  product!.quantity -= sale.quantity;
  await shop.save();
  
  const updated = await saleRepo.create(sale)
  return updated
}



const findByIdAndUpdate = (filter: FilterQuery<ISale>,update: UpdateQuery<ISale>,options: QueryOptions = {}
) => {
  return saleRepo.findByIdAndUpdate(filter, update);
};

const find = async (query:any) => {
  const products = await saleRepo.find(query);
  return products;
};


const deleteSale = async (saleId: string) => {
  const sale = await saleRepo.findById(saleId);
  if (!sale) {
    throw SALE_RESPONSES.NOT_FOUND;
  }

  const shop = await shopServices.findById(sale.shopid);
  const product = shop.products.find((p) => p._id.toString() == sale.product_id);
  
  if (!product) {
    throw SALE_RESPONSES.PRODUCT_NOT_FOUND;
  }

  product.quantity += sale.quantity;
  await shop.save();

 const record = await saleRepo.update(
    { _id: new mongoose.mongo.ObjectId(saleId) },
    { isDeleted: true }
  );
  if (!record) throw SALE_RESPONSES.NOT_FOUND;
  return record;
};


const ViewPendingSales = async () => {
  const Sales = await saleRepo.ViewPendingSales();
  return Sales
}


const approveSale = async (shopId:string) => {
  const {pendingRevenue, saleRecords} = await saleRepo.ViewPendingSales();
  const salepoints = pendingRevenue[0].totalPoints;

  const {approve,totalRevenue} = await saleRepo.approveSale();
  
  const shop = await shopServices.findOne({_id:shopId})
  if(shop){
    shop.points += salepoints
    shop.totalRevenue += totalRevenue;
    await shop.save();
  }
  else{
    throw SALE_RESPONSES.NOT_FOUND
  }
  return salepoints 
}

const itemwise = async () => {
  saleRepo.itemwise()
}

const rejectSale = async(shopId:string) =>{
  const shop = await shopServices.findOne({ _id: shopId });
  const reject = await saleRepo.rejectSale();
  return reject
}

export default{
    sellProduct,
    findByIdAndUpdate,
    ViewPendingSales,
    approveSale,
    rejectSale,
    itemwise,
    deleteSale,
    find
}