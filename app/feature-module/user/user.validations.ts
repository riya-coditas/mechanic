import { body, param, query } from "express-validator";
import { validate } from "../../utils/validate";

export const USER_VALIDATION = [
  body("name")
  .isString()
  .withMessage("String value required"),
];

export const PARAM_VALIDATION = [
  param("id")
  .isString()
  .withMessage("id parameter is necessary"),

  validate,
];

export const FINDALL_USER_VALIDATION = [
  query("sort")
  .optional()
  .isString()
  .withMessage("sort field to sort"),

  query("limit")
  .optional()
  .isNumeric()
  .withMessage("limit field to limit"),

  query("page")
  .optional()
  .isNumeric()
  .withMessage("page field to paginate"),
  
  validate,
];