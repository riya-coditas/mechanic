import mongoose, { FilterQuery, ObjectId, QueryOptions, UpdateQuery } from "mongoose";
import userRepo from "./user.repo";
import { IUser } from "./user.type"
import { USER_RESPONSE } from "./user.responses";
import { UserModel } from "./user.schema";


const create = (user:IUser) => userRepo.create(user);

const find = ()=>userRepo.find();

const findOne = async (filters:Partial<IUser>) => {
    const user = await userRepo.findOne(filters);
    if(!user) throw USER_RESPONSE.NOT_FOUND
    
    return user;
}

const findByIdAndUpdate = (filter: FilterQuery<IUser>,update: UpdateQuery<IUser>,options: QueryOptions = {}
) => {
  return userRepo.findByIdAndUpdate(filter, update, { ...options, new: true });
};

const deleteUser = async (id: string) => {
  const record = await userRepo.update(
    { _id: new mongoose.mongo.ObjectId(id) },
    { isDeleted: true }
  );
  if (!record) throw USER_RESPONSE.NOT_FOUND;
  return record;
};


export default {
    create,
    find,
    findOne,
    findByIdAndUpdate,
    deleteUser
}