import { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import {UserModel} from "./user.schema"
import { IUser } from "./user.type"

const create = (user:IUser) => UserModel.create(user);

const find = async () => {
  try {
    return await UserModel.find({
      isDeleted: false,
    });
  } catch (err) {
    throw { message: "something went wrong", e: err };
  }
};

const findOne = async (filters:Partial<IUser>) => {
    try {
        return await UserModel.findOne({
            ...filters,
            isDeleted:false
        })
    } catch (err) {
        throw { message: 'something went wrong', e: err } 
    } 
}

const findByIdAndUpdate = (filter: FilterQuery<IUser>,update: UpdateQuery<IUser>,options: QueryOptions = {}
) => {
  return UserModel.findByIdAndUpdate(filter, update, {...options,new: true,});
};

const update = async (filter: FilterQuery<IUser>, update: UpdateQuery<IUser>) =>
  await UserModel.updateOne(filter, update);


export default {
    create,
    find,
    findOne,
    findByIdAndUpdate,
    update
}