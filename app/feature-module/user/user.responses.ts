export const USER_RESPONSE = {
    NOT_FOUND: {
        statusCode: 404,
        message: 'user not found'
    }
}