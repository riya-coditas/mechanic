import { Router,Request,Response,NextFunction } from "express";
import { ResponseHandler } from "../../utils/response_handler";
import { Roles } from "../role/role.types";
import userServices from "./user.services";
import mongoose from "mongoose";
import { permit } from "../../utils/permit";
import { FINDALL_USER_VALIDATION, PARAM_VALIDATION } from "./user.validations";


const router = Router();

router.get("/",FINDALL_USER_VALIDATION,permit([Roles.admin.toString()]), async (req:Request, res:Response, next:NextFunction) => {
    try {
      const result = await userServices.find();
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.patch("/Update/:id",PARAM_VALIDATION,permit([Roles.admin.toString()]),async (req:Request, res:Response, next:NextFunction) => {
    try {
      const id = req.params.id;
      const update = req.body;
      const result = await userServices.findByIdAndUpdate(new mongoose.mongo.ObjectId(id),
        update
      );
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.delete("/Delete/:id",PARAM_VALIDATION,permit([Roles.admin.toString()]),
  async (req:Request, res:Response, next:NextFunction) => {
    try {
      const id = req.params.id;
      const result = await userServices.deleteUser(id);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

export default router


