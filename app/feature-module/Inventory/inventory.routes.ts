import { NextFunction, Request, Response, Router } from "express";
import { ResponseHandler } from "../../utils/response_handler";
import productServices from "./inventory.services";
import { Roles } from "../role/role.types";
import mongoose from "mongoose";
import { FINDALL_PRODUCT_VALIDATION, PARAM_VALIDATION, ProductValidator } from "./inventory.validations";
import { permit } from "../../utils/permit";

const router = Router()


router.get("/", FINDALL_PRODUCT_VALIDATION, permit([Roles.admin.toString()]),async (req:Request, res:Response, next:NextFunction) => {
    try {
      const filter = req.query
      const result = await productServices.find(filter);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.post("/createProduct",permit([Roles.admin.toString()]), ProductValidator , async (req:Request,res:Response,next:NextFunction) => {
    try{
        const result = await productServices.create(req.body);
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }    
})


router.patch("/Update/:id", PARAM_VALIDATION ,permit([Roles.admin.toString()]),async (req:Request, res:Response, next:NextFunction) => {
    try {
      const id = req.params.id;
      const update = req.body;
      const result = await productServices.findByIdAndUpdate(new mongoose.mongo.ObjectId(id), update);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.delete("/Delete/:id",PARAM_VALIDATION,permit([Roles.admin.toString()]),async (req:Request, res:Response, next:NextFunction) => {
    try {
      const id = req.params.id;
      const result = await productServices.deleteProduct(id);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);


export default router;