import { Schema, model, Document } from "mongoose";
import { BaseSchema } from "../../utils/base-schema";
import { IProduct } from "./inventory.types";


export const ProductSchema = new BaseSchema({
    name : {
        type : String,
        required : true
    },
    price : {
        type : Number,
        required : true
    },
    quantity : {
        type : Number,
        required : true
    },
    Threshold : {
        type : Number,
    },
    Productpoints : {
        type: Number,
        default: 0
    }
})

type ProductDocument = Document & IProduct;

export const ProductModel = model<ProductDocument>("Product",ProductSchema);
