import mongoose, { FilterQuery, PipelineStage, QueryOptions, UpdateQuery } from "mongoose";
import productRepo from "./inventory.repo";
import { IProduct } from "./inventory.types";
import { PRODUCT_RESPONSES } from "./inventory.responses";
import { generatePipeline } from "../../utils/pipeline";
import { ProductModel } from "./inventory.schema";


const create = (product : IProduct) => productRepo.create(product);

const findOne = async (filters:Partial<IProduct>) => {
    const product = await productRepo.findOne(filters);
    if(!product) throw "Not found"
    
    return product;
}

const findByIdAndUpdate = (filter: FilterQuery<IProduct>,update: UpdateQuery<IProduct>, options: QueryOptions = {}
) => {
  return productRepo.findByIdAndUpdate(filter, update);
};

const find = async (query:any) => {
  const products = await productRepo.find(query);
  return products;
};


const deleteProduct = async (id: string) => {
  const record = await productRepo.update(
    { _id: new mongoose.mongo.ObjectId(id) },
    { isDeleted: true }
  );
  if (!record) throw PRODUCT_RESPONSES.NOT_FOUND;
  return record;
};


export default{
    create,
    findOne,
    find,
    findByIdAndUpdate,
    deleteProduct,
}