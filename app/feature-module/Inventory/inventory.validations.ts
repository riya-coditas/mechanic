import { body, param, query } from "express-validator";
import { validate } from "../../utils/validate";


export const ProductValidator = [
  body("name")
  .isString()
  .withMessage("String value required"),

  body("price")
  .isInt()
  .withMessage("Numeric value required"),

  body("quantity")
  .isInt()
  .withMessage("Numeric value required"),

  body("Productpoints").optional(),
    
  body("Threshold")
  .isInt()
  .withMessage("Numeric value required"),

  validate,
];

export const FINDALL_PRODUCT_VALIDATION = [
  query("sort")
  .optional()
  .isString()
  .withMessage("sort field to sort"),

  query("limit")
  .optional()
  .isNumeric()
  .withMessage("limit field to limit"),

  query("page")
  .optional()
  .isNumeric()
  .withMessage("page field to paginate"),

  validate,
];

export const PARAM_VALIDATION = [
  param("id")
  .isString()
  .withMessage("id parameter is necessary"),
  
  validate,
];