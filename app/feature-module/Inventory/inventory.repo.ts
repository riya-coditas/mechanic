import mongoose, { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import { ProductModel } from "./inventory.schema";
import { IProduct } from "./inventory.types";
import { generatePipeline } from "../../utils/pipeline";

const create = (product: IProduct) => ProductModel.create(product);

const findOne = async (filters: Partial<IProduct>) => {
  try {
    return await ProductModel.findOne({
      ...filters,
      isDeleted: false,
    });
  } catch (err) {
    throw { message: "something went wrong", e: err };
  }
};

const findByIdAndUpdate = (
  filter: FilterQuery<IProduct>,
  update: UpdateQuery<IProduct>,
  options: QueryOptions = {}
) => {
  return ProductModel.findByIdAndUpdate(filter, update, {
    ...options,
    new: true,
  });
};

const find = async (query:any) => {
  try {
    const pipeline = generatePipeline(query);
    return await ProductModel.aggregate([
      {
        $match: { isDeleted: false },
      },
      ...pipeline,
    ]);
  } catch (err) {
    throw { message: "something went wrong", e: err };
  }
};


const update = async (
  filter: FilterQuery<IProduct>,
  update: UpdateQuery<IProduct>
) => await ProductModel.updateOne(filter, update);

export default {
  create,
  findOne,
  find,
  update,
  findByIdAndUpdate
};
