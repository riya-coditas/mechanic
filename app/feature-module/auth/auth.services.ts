import { compare, genSalt, hash } from "bcryptjs";
import userServices from "../user/user.services";
import { IUser } from "../user/user.type";
import { AUTH_RESPONSES } from "./auth.responses";
import { ICredentials, Payload } from "./auth.types";
import { Roles } from "../role/role.types";
import jwt, { sign, verify } from "jsonwebtoken";
import fs from "fs";
import path from "path";


const encryptUserPassword = async (user : IUser) => {
    const salt = await genSalt(10)
    const hashedPassword = await hash(user.password,salt)
    user.password = hashedPassword;

    return user
}


const register = async(user : IUser) => {
  user = await encryptUserPassword(user)
  if(!user.role){
    user.role = Roles.owner
  }
  const record = userServices.create(user) 
  return record  
}



const login = async (credentials: ICredentials) => {
    const user = await userServices.findOne({email:credentials.email});
    if(!user) throw AUTH_RESPONSES.INVALID_CREDENTIALS;

    const isPasswordValid = await compare(credentials.password, user.password);
    if(!isPasswordValid) throw AUTH_RESPONSES.INVALID_CREDENTIALS;

    const { _id, role } = user;

    const PRIVATE_KEY = fs.readFileSync(path.resolve(__dirname, "..\\..\\keys\\private.pem"),
      { encoding: "utf-8" });
    try {
        var token = jwt.sign({ id: _id, role: role }, PRIVATE_KEY || "", {
        algorithm: "RS256", expiresIn:'9000s'
      });
        var refreshToken = jwt.sign({ id: _id, role: role }, PRIVATE_KEY || "", {
          algorithm: "RS256", expiresIn:'9000s',
        });
        
      return { token, role,refreshToken };
    } catch (error) {
      console.log(error);
    }
  }   
//  const {JWT_SECRET} = process.env;
//  const {_id,role} = user;

//  const token = sign({id:_id,role:role},JWT_SECRET||"");
//  return {token,role};

const refreshToken = (token: string) => {
  const PUBLIC_KEY = fs.readFileSync(
    path.resolve(__dirname, "..\\..\\keys\\public.pem"),
    { encoding: "utf-8" }
  );
  const tokenDecode = verify(token || "", PUBLIC_KEY || "") as Payload;
  console.log(tokenDecode);
  const PRIVATE_KEY = fs.readFileSync(
    path.resolve(__dirname, "..\\..\\keys\\private.pem"),
    { encoding: "utf-8" }
  );
  if (tokenDecode) {
    const accessToken = jwt.sign(
      { id: tokenDecode.id, role: tokenDecode.role },
      PRIVATE_KEY || "",
      { algorithm: "RS256", expiresIn: "9000s" }
    );
    return { accessToken };
  } else {
    throw { statusCode: 400, message: "Token invalid" };
  }
};


export default{
    register,
    login,
    refreshToken
}

