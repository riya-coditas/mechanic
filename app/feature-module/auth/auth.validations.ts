import { body } from "express-validator";
import { validate } from "../../utils/validate";

export const LoginValidator = [
    body("email")
    .isEmail()
    .isLength({min : 6})
    .withMessage("Email is Required"),

    body("password")
    .isString()
    .isLength({ min : 6})
    .withMessage("Password is Required"),
    
    validate,
]