import { Router,Request,Response,NextFunction } from "express";
import { ResponseHandler } from "../../utils/response_handler";
import authServices from "./auth.services";
import { LoginValidator } from "./auth.validations";


const router = Router();

router.post("/register",  LoginValidator, async(req:Request,res:Response,next:NextFunction)=> {

    try{
        const user = req.body
        const result = await authServices.register(user)
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
});

router.post("/login", LoginValidator, async (req:Request,res:Response,next:NextFunction) => {

    try{
        const credentials = req.body
        const result = await authServices.login(credentials)
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }

 next();

});

router.post("/refresh",async (req, res, next) => {
    try {
      const refreshToken = req.body.refreshToken;
      const result = authServices.refreshToken(refreshToken);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }

    next();
  }
);

export default router;