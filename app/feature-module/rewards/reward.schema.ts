import { Schema, model, Document } from "mongoose";
import { BaseSchema } from "../../utils/base-schema";
import { IReward } from "./reward.types";

export const RewardSchema = new BaseSchema({

    name : {
        type : String,
        required : true
    },
    points:{
        type : Number,
        required : true
    }
})

type RewardDocument = Document & IReward;

export const RewardModel = model<RewardDocument>("Reward",RewardSchema);
