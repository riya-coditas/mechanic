import { NextFunction, Request, Response, Router } from "express";
import { ResponseHandler } from "../../utils/response_handler";
import rewardServices from "./reward.services";
import { Roles } from "../role/role.types";
import mongoose from "mongoose";
import { permit } from "../../utils/permit";
import { PARAM_VALIDATION, RewardValidator } from "./reward.validations";

const router = Router()

router.get("/", permit([Roles.admin.toString(),Roles.owner.toString()]),  async (req, res, next) => {
    try {
      const filter = req.query;
      const result = await rewardServices.find(filter);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.post("/create", RewardValidator ,permit([Roles.admin.toString()]) ,async (req:Request,res:Response,next:NextFunction) => {
    try{
        const result = await rewardServices.create(req.body);
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    } 
})

router.patch("/Update/:id", PARAM_VALIDATION, permit([Roles.admin.toString()]),async (req:Request, res:Response, next:NextFunction) => {
    try {
      const id = req.params.id;
      const update = req.body;
      const result = await rewardServices.findByIdAndUpdate(new mongoose.mongo.ObjectId(id),update
      );
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.delete("/Delete/:id",PARAM_VALIDATION,permit([Roles.admin.toString()]),
  async (req:Request, res:Response, next:NextFunction) => {
    try {
      const id = req.params.id;
      const result = await rewardServices.deleteReward(id);
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

router.get("/UpcomingReward/:id",PARAM_VALIDATION,permit([Roles.owner.toString()]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const id = req.params.id;
      const result = await rewardServices.findUpcomingReward(id)
      res.send(new ResponseHandler(result));
    } catch (e) {
      next(e);
    }
  }
);

export default router;