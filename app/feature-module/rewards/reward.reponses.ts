export const REWARD_RESPONSES = {
  NOT_FOUND: {
    statusCode: 404,
    message: "Reward Not Found",
  },

  SHOP_NOT_FOUND: {
    statusCode: 404,
    message: "Shop Not Found",
  },
};