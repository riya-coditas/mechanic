import { body, param } from "express-validator";
import { validate } from "../../utils/validate";

export const RewardValidator = [
  body("name")
  .isString()
  .withMessage("String value required"),

  body("points")
  .isInt()
  .withMessage("Numeric value required"),

  validate,
];

export const PARAM_VALIDATION = [
  param("id")
  .isString()
  .withMessage("id parameter is necessary"),
  validate,
];
