import { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import { RewardModel } from "./reward.schema";
import { IReward } from "./reward.types";
import { generatePipeline } from "../../utils/pipeline";
import shopServices from "../shop/shop.services";
import { REWARD_RESPONSES } from "./reward.reponses";


const create = (reward : IReward) => RewardModel.create(reward);

const findById = async (id: string) => {
  const reward = await RewardModel.findById(id);
  return reward;
};

const findByIdAndUpdate = (filter: FilterQuery<IReward>,update: UpdateQuery<IReward>,options: QueryOptions = {}
) => {
  return RewardModel.findByIdAndUpdate(filter, update, {
    ...options,
    new: true,
  });
};

const findOne = async (filters: Partial<IReward>) => {
  try {
    return await RewardModel.findOne({
      ...filters,
      isDeleted: false,
    });
  } catch (err) {
    throw { message: "something went wrong", e: err };
  }
};

const find = async (query: any) => {
  try {
    const pipeline = generatePipeline(query);
    return await RewardModel.aggregate([
      {
        $match: { isDeleted: false },
      },
      ...pipeline,
    ]);
  } catch (err) {
    throw { message: "something went wrong", e: err };
  }
};

const update = async (
  filter: FilterQuery<IReward>,
  update: UpdateQuery<IReward>
) => await RewardModel.updateOne(filter, update);


const findUpcomingReward = async (id: string) => {
  const shop = await shopServices.findById(id);
  if (!shop) {
    throw REWARD_RESPONSES.SHOP_NOT_FOUND;
  }

  const upcomingReward = await RewardModel.aggregate([
    {
      $match: { points: { $gt: shop.points } },
    },
    {
      $limit: 1,
    },
  ]);
  if(!shop.points){
    throw REWARD_RESPONSES.SHOP_NOT_FOUND
  }  
  const pointsLeft = upcomingReward[0].points - shop.points 

  return [upcomingReward[0],"You have "+pointsLeft+" points left to redem the next Reward"];
};

export default{
    create,
    findById,
    findOne,
    find,
    findByIdAndUpdate,
    update,
    findUpcomingReward
}