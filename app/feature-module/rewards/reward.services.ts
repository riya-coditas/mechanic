import mongoose, { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import rewardRepo from "./reward.repo";
import { IReward } from "./reward.types";
import { REWARD_RESPONSES } from "./reward.reponses";


const create = (reward : IReward) => rewardRepo.create(reward);

const findById = async (id: string) => {
  const reward = await rewardRepo.findById(id);
  return reward;
};

const findByIdAndUpdate = (filter: FilterQuery<IReward>,update: UpdateQuery<IReward>,options: QueryOptions = {}
) => {
  return rewardRepo.findByIdAndUpdate(filter, update);
};

const findOne = async (filters: Partial<IReward>) => {
  const reward = await rewardRepo.findOne(filters);
  if (!reward) throw REWARD_RESPONSES.NOT_FOUND;

  return reward;
};


const find = async (query:any) => {
  const rewards = await rewardRepo.find(query);
  return rewards;
};

const deleteReward = async (id: string) => {
  const record = await rewardRepo.update(
    { _id: new mongoose.mongo.ObjectId(id) },
    { isDeleted: true }
  );
  if (!record) throw REWARD_RESPONSES.NOT_FOUND;
  return record;
};

const findUpcomingReward = async (id:string) =>{
  const upcomingReward = rewardRepo.findUpcomingReward(id)
  return upcomingReward
}

export default{
    create,
    findById,
    findOne,
    find,
    findByIdAndUpdate,
    deleteReward,
    findUpcomingReward
}