export const generatePipeline = (query: any) => {
  let pipeline: any = [];
  const { sort, limit = 10, sortDir, page = 1, ...filter } = query;
  let filterObject: Record<string, any> = {};

  for (let field in filter) {
    if (Array.isArray(filter[field])) {
      filterObject[field] = {
        $in: filter[field],
      };
    } else if (typeof filter[field] === "object") {
      let obj: Record<string, any> = {};
      for (let [key, value] of Object.entries(filter[field])) {
        obj[`$${key}`] = typeof value === "number" ? value : Number(value);
      }
      filterObject[field] = obj;
    } else {
      const parsedValue = isNaN(Number(filter[field]))
        ? filter[field]
        : Number(filter[field]);
      filterObject[field] = parsedValue;
    }
  }

  if (Object.keys(filterObject).length > 0) {
    pipeline.push({ $match: filterObject });
  }

  const sortObject: Record<string, any> = {};
  if (sort && sortDir) {
    sortObject[sort] = sortDir === "asc" ? 1 : -1;
    pipeline.push({ $sort: sortObject });
  }

  const skip = (page - 1) * limit;
  pipeline.push({ $skip: skip });
  pipeline.push({ $limit: Number(limit) });

  return pipeline;
};
