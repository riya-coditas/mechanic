import { Router } from "express";

export class Routes {

    constructor(
        public path : string,
        public router : Router
    ){}
}

export type Route = Routes[]