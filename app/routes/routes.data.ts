import {Route, Routes} from "./routes.type"
import routeProvider from "../feature-module/route-provider"
import { ExcludedPath, ExcludedPaths } from "../utils/authorize"

export const routes: Route = [
  new Routes("/auth", routeProvider.authRouter),
  new Routes("/users",routeProvider.userRouter),
  new Routes("/roles", routeProvider.roleRouter),
  new Routes("/product", routeProvider.productRouter),
  new Routes("/shop", routeProvider.shopRouter),
  new Routes("/sales", routeProvider.saleRouter),
  new Routes("/requirement", routeProvider.requirementRouter),
  new Routes("/rewards", routeProvider.rewardRouter),
];

export const excludedPaths: ExcludedPaths = [
    new ExcludedPath("/auth/login", "POST"),
    new ExcludedPath("/auth/register", "POST"),
    new ExcludedPath("/shop/","GET"),
    new ExcludedPath("/ratings/rating","POST"),
    new ExcludedPath("/auth/refresh","POST")
];